//Ilaria Teodori

public class Ray{
  
  // direzione raggio
  Point rd;

  // origine del raggio
  Point ro;

  public Ray(Point origin, Point direction){
    //costruttore
      rd = direction.min(origin);

      ro = origin;
  }

  public boolean getVisibility(Scene scn, int i){
    double t0;
    t0 = this.intersect(scn.obj.get(i));
    if( (t0<0.001 || t0>0.9)  ) return true;
    else return false;
  }

  public boolean getVisibility2(Sphere[] bodies, int i){
	    double t0;
	    t0 = this.intersect(bodies[i]);
	    if( (t0<0.001 || t0>0.9)  ) return true;
	    else return false;
	  }
  
  public double intersect(Sphere ob){
    Point o_c = ro.min(ob.center);
    double b = rd.dot(o_c);
    double a = rd.dot(rd);
    double c = o_c.dot(o_c) - ob.size*ob.size;

    double delta = b*b - a*c;
    if(delta<0)
      return -10;
    delta = Math.sqrt(delta);
    double t1 = (-b + delta) / a;
    double t2 = (-b - delta) / a;
    if(t1<0) t1 = t2;
    if(t2<0) t2 = t1;
    double t = (t1<t2)? t1 : t2;
    if(t<0) return -20;
    return t;
  }


  public double intersect(Triangle tr){
    
    Point N = tr.getNormal();
    double rdn = N.dot(this.rd);
    double d = N.dot(tr.a);
    double t = (d - this.ro.dot(N)) / rdn;
    if(t >= 0.99 || t<0.01 ){
     return 0;
    }
    return t;
  }
}

