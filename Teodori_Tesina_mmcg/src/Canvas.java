import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

public class Canvas extends JPanel{

private static final long serialVersionUID = 1L;

/*
 Pannello della visione prospettica su cui viene calcolato lo Z-Buffer del RenderingEngine
 
 
*/
		public int width, height;
		public static int t;
		public BufferedImage image;
		public RenderingEngine rnd;

		public Canvas(int w, int h, RenderingEngine rnd) {
			super();
			width = w;
			height = h;
			this.setPreferredSize(new Dimension(width, height));
			this.setVisible(true);
			this.rnd=rnd;
		}

		@Override
		public void paintComponent(Graphics gr) {
			super.paintComponent(gr);

			image = rnd.render();

			gr.drawImage(image, 0, 0, null);
//			rnd.scn.lgt.get(0).rotateY(0);
//			rnd.scn.lgt.get(1).rotateY(0);
			if(rnd.texture_mapping)
			rnd.scn.lgt.get(1).rotateY(0.00595);
			rnd.scn.simulate();

			t += 1; // incremento la variabile temporale ad ogni frame
		}

	
	
}
