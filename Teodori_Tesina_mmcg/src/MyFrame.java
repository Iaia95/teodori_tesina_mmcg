import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class MyFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	static final int numPlanets = 6;

	public static UpPanel titleContainer = new UpPanel();
	public static TitlePanel title = new TitlePanel();
	public static ButtonPanel buttons = new ButtonPanel();
	public static HomePanel home = new HomePanel();
	public static OrthoProjections o_pj;
	public static HelpPanel help = new HelpPanel();
	public static PlanetPanel planet = new PlanetPanel();
	public static PlanetsContainerPanel planetH = new PlanetsContainerPanel();
	public static HomeOptionsPanel home_opz = new HomeOptionsPanel();

	public static Canvas canvas;
	public static RenderingEngine _rnd;
	public static Scene _scn;

	public static Sphere[] _planets;

	public MyFrame(String s, int width, int height, Sphere[] pianeti) {
		super(s);// costruttore della classe genitore
		setTitle("Render");
		setSize(1500, 900);
		setLayout(new BorderLayout());
		_planets = pianeti;
		
		
		o_pj = new OrthoProjections(width, height, _planets);
		
		add(title);
		add(titleContainer);
		add(buttons);
		add(home_opz);
		add(home);
		add(o_pj);
		o_pj.setVisible(false);
		add(help);
		help.setVisible(false);
		add(planet);
		planet.setVisible(false);
		add(planetH);
		planetH.setVisible(false);

		_scn = new Scene(0.05);
		// Creo e aggiungo le sorgenti di luce
		Light l1 = new Light(-366, -166, 800, 300);
		Light l2 = new Light(3000, 0, 0, 1);
		l2.l = 0.0;
		l2.c = 1.0;
		l1.l = 0.5;
		l2.q = l1.q = 0;
		_scn.lgt.add(l1);
		_scn.lgt.add(l2);
		for (int i = 0; i < numPlanets; i++) {
			_scn.addSphere(_planets[i]);
		}

		_rnd = new RenderingEngine(width, height, 850, new Point(0, 0, -1200), new Point(0, 0, 1), _scn);
		_rnd.texture_mapping = false;

		canvas = new Canvas(width, height, _rnd);
		canvas.setPreferredSize(new Dimension(1500, 900));
		canvas.setVisible(true);
		this.add(canvas);

		this.addMouseListener(new MouseListener() {

			public void mousePressed(MouseEvent e) {
				// System.out.println("Mouse pressed; # of clicks: ");
			}

			public void mouseReleased(MouseEvent e) {
				// System.out.println("Mouse released; # of clicks: ");
			}

			public void mouseEntered(MouseEvent e) {
				// System.out.println("Mouse entered");
			}

			public void mouseExited(MouseEvent e) {
				// System.out.println("Mouse exited");
			}

			public void mouseClicked(MouseEvent e) {
				Point p = _rnd.camera.sum(e.getX() - _rnd.w_2, e.getY() - _rnd.h_2, 0);
				p.z = 800;
				_scn.lgt.get(0).set(p);
			}
		});

		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher() {
			public double vx = 0.0, vy = 0.0, vz = 0.0;
			public int focal;

			@Override
			public boolean dispatchKeyEvent(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_UP)
					vy = -10;
				if (e.getKeyCode() == KeyEvent.VK_DOWN)
					vy = 10;
				if (e.getKeyCode() == KeyEvent.VK_LEFT)
					vx = -10;
				if (e.getKeyCode() == KeyEvent.VK_RIGHT)
					vx = 10;
				if (e.getKeyCode() == KeyEvent.VK_F) {
					vz = 10;
				}
				if (e.getKeyCode() == KeyEvent.VK_B) {
					vz = -10;
				}

				_rnd.shiftCamera(new Point(vx, vy, vz));
				vx = 0;
				vy = 0;
				vz = 0;
				return false;
			}
		});

		this.setFocusable(true);
		this.setAutoRequestFocus(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);

	}

}

class UpPanel extends JPanel { // pannello titolo
	private static final long serialVersionUID = 1L;

	public UpPanel() {
		setBounds(0, 0, 850, 80);
	}
}

class TitlePanel extends JPanel { // pannello interno al pannello titolo

	private static final long serialVersionUID = 1L;
	JLabel etichetta = new JLabel("SCIENZE E TECNOLOGIE PER I MEDIA");
	JLabel etichetta2 = new JLabel("Metodi Matematici per Computer Graphics");

	public TitlePanel() {
		BufferedImage myPicture;
		JLabel picLabel = new JLabel();
		try {
			ClassLoader classLoader = this.getClass().getClassLoader();
			InputStream is = classLoader.getResourceAsStream("images/Uniroma22.png");
			myPicture = ImageIO.read(is);

			picLabel = new JLabel(new ImageIcon(myPicture));
		} catch (IOException e) {
			e.printStackTrace();
		}
		setBounds(0, 10, 800, 55);
		add(picLabel);
		add(etichetta);
		add(etichetta2);
		etichetta.setFont(new Font("SansSerif", Font.BOLD, 20));
		etichetta2.setFont(new Font("SansSerif", Font.BOLD, 15));
	}
}

class ButtonPanel extends JPanel {// pannello bottoni home - menu'

	private static final long serialVersionUID = 1L;
	JButton home = new JButton("Home");
	JButton ortho = new JButton("OrthoPanels");
	JButton pianeti = new JButton("Pianeti");
	JButton help = new JButton("Help");
	JButton exit = new JButton("Exit");

	public ButtonPanel() {
		setBounds(800, 10, 500, 65);

		add(home);
		add(ortho);
		add(pianeti);
		add(help);
		add(exit);

		MenuButtonAction homeAL = new MenuButtonAction();
		home.addActionListener(homeAL);

		MenuButtonAction orthoAL = new MenuButtonAction();
		ortho.addActionListener(orthoAL);

		MenuButtonAction pianetiAL = new MenuButtonAction();
		pianeti.addActionListener(pianetiAL);

		MenuButtonAction helpAL = new MenuButtonAction();
		help.addActionListener(helpAL);

		MenuButtonAction exitAL = new MenuButtonAction();
		exit.addActionListener(exitAL);
	}
}

class HomePanel extends JPanel {// pannello container home

	private static final long serialVersionUID = 1L;

	public HomePanel() {
		Label etichetta = new Label("HOME");
		etichetta.setFont(new Font("SansSerif", Font.BOLD, 17));

		setBorder(BorderFactory.createLineBorder(Color.black));
		setBounds(850, 80, 500, 600);

		add(etichetta);
	}
}

class HomeOptionsPanel extends JPanel { // pannello home con opzioni

	private static final long serialVersionUID = 1L;
	JPanel texture = new JPanel();
	JPanel shading = new JPanel();
	JPanel gouraud = new JPanel();
	JPanel integ = new JPanel();
	JPanel light = new JPanel();
	JPanel ifTexture = new JPanel();

	Label txtr = new Label("Show Texture: ");
	Label shd = new Label("Shading type: ");
	Label grd = new Label("Gouraud shading: ");
	Label alg = new Label("Algorithm: ");
	Label lgt = new Label("Luce 2: posizionare con click sinistro");
	Label ifTxtr = new Label("Solo con texture attiva: ");

	JButton texture_opz1 = new JButton("off");
	JButton texture_opz2 = new JButton("planets");
	JButton texture_opz3 = new JButton("chess");
	JButton texture_opz4 = new JButton("stripes");
	JButton shading_opz1 = new JButton("off");
	JButton shading_opz2 = new JButton("Phong");
	JButton shading_opz3 = new JButton("Blinn");
	JButton gouraud_opz1 = new JButton("off");
	JButton gouraud_opz2 = new JButton("on");
	JCheckBox integ_opz1 = new JCheckBox("Runge Kutta 4", true);
	JCheckBox integ_opz2 = new JCheckBox("Eulero", false);
	JButton light_opz1 = new JButton("off");
	JButton light_opz2 = new JButton("on");
	JCheckBox nuvole = new JCheckBox("Nuvole");
	JCheckBox prec = new JCheckBox("Precessione Equinozi");

	public double intensity;

	public HomeOptionsPanel() {
		setBounds(880, 150, 450, 500);
		setFont(new Font("SansSerif", Font.BOLD, 10));

		texture.setPreferredSize(new Dimension(450, 70));
		txtr.setFont(new Font("SansSerif", Font.BOLD, 14));
		texture.add(txtr);
		texture.add(texture_opz1);
		texture.add(texture_opz2);
		texture.add(texture_opz3);
		texture.add(texture_opz4);
		add(texture);

		ifTexture.setPreferredSize(new Dimension(450, 70));
		ifTxtr.setFont(new Font("SansSerif", Font.BOLD, 14));
		ifTexture.add(ifTxtr);
		ifTexture.add(nuvole);
		nuvole.setEnabled(false);
		ifTexture.add(prec);
		prec.setEnabled(false);
		add(ifTexture);

		shading.setPreferredSize(new Dimension(450, 70));
		shd.setFont(new Font("SansSerif", Font.BOLD, 14));
		shading.add(shd);
		shading.add(shading_opz1);
		shading.add(shading_opz2);
		shading.add(shading_opz3);
		add(shading);

		gouraud.setPreferredSize(new Dimension(450, 70));
		grd.setFont(new Font("SansSerif", Font.BOLD, 14));
		gouraud.add(grd);
		gouraud.add(gouraud_opz1);
		gouraud.add(gouraud_opz2);
		add(gouraud);

		integ.setPreferredSize(new Dimension(450, 70));
		alg.setFont(new Font("SansSerif", Font.BOLD, 14));
		integ.add(alg);
		integ.add(integ_opz1);
		integ.add(integ_opz2);
		add(integ);
		integ_opz1.setEnabled(false);
		integ_opz1.setSelected(true);

		light.setPreferredSize(new Dimension(450, 70));
		lgt.setFont(new Font("SansSerif", Font.BOLD, 14));
		light.add(lgt);
		light.add(light_opz1);
		light.add(light_opz2);
		add(light);

		texture_opz1.addActionListener(new ActionListener() { // Texture OFF button

			@Override
			public void actionPerformed(ActionEvent e) {
				MyFrame._rnd.texture_mapping = false;
				nuvole.setEnabled(false);
				nuvole.setSelected(false);
				for (int i = 0; i < MyFrame.numPlanets; i++) {
					MyFrame._planets[i].precessione = false;
				}
				prec.setEnabled(false);
				prec.setSelected(false);
				Sphere.c = false;

			}
		});

		texture_opz2.addActionListener(new ActionListener() { // Texture planets button

			@Override
			public void actionPerformed(ActionEvent e) {
				for (int i = 0; i < MyFrame.numPlanets; i++) {
					MyFrame._planets[i].setTexture(MyFrame._planets[i].texture_name);
				}
				MyFrame._rnd.texture_mapping = true;
				nuvole.setEnabled(true);
				prec.setEnabled(true);
				nuvole.setSelected(false);
				prec.setSelected(false);
				Sphere.c = false;
				for (int i = 0; i < MyFrame.numPlanets; i++) {
					MyFrame._planets[i].precessione = false;
				}

			}
		});

		texture_opz3.addActionListener(new ActionListener() { // Texture 'chess' button

			@Override
			public void actionPerformed(ActionEvent e) {
				for (int i = 0; i < MyFrame.numPlanets; i++) {
					MyFrame._planets[i].setTexture(MyFrame._planets[i].texture_chess);
				}
				MyFrame._rnd.texture_mapping = true;
				nuvole.setEnabled(true);
				prec.setEnabled(true);
				nuvole.setSelected(false);
				prec.setSelected(false);
				Sphere.c = false;
				for (int i = 0; i < MyFrame.numPlanets; i++) {
					MyFrame._planets[i].precessione = false;
				}

			}
		});
		
		texture_opz4.addActionListener(new ActionListener() { // Texture 'stripes' button

			@Override
			public void actionPerformed(ActionEvent e) {
				for (int i = 0; i < MyFrame.numPlanets; i++) {
					MyFrame._planets[i].setTexture(MyFrame._planets[i].texture_stripes);
				}
				MyFrame._rnd.texture_mapping = true;
				nuvole.setEnabled(true);
				prec.setEnabled(true);
				nuvole.setSelected(false);
				prec.setSelected(false);
				Sphere.c = false;
				for (int i = 0; i < MyFrame.numPlanets; i++) {
					MyFrame._planets[i].precessione = false;
				}

			}
		});

		Sphere.c = false;
		nuvole.addActionListener(new ActionListener() { // nuvole ON-OFF

			@Override
			public void actionPerformed(ActionEvent event) {
				JCheckBox cb = (JCheckBox) event.getSource();
				if (cb.isSelected()) {
					Sphere.c = true;

				} else {
					Sphere.c = false;
				}
			}
		});

		prec.addActionListener(new ActionListener() { // precessione

			@Override
			public void actionPerformed(ActionEvent event) {
				JCheckBox cb = (JCheckBox) event.getSource();
				if (cb.isSelected()) {
					for (int i = 0; i < MyFrame.numPlanets; i++) {
						MyFrame._planets[i].precessione = true;
					}
				} else {
					for (int i = 0; i < MyFrame.numPlanets; i++) {
						MyFrame._planets[i].precessione = false;
					}
				}
			}
		});

		shading_opz1.addActionListener(new ActionListener() { // No z-shading

			@Override
			public void actionPerformed(ActionEvent event) {
				MyFrame._rnd.zShading = true;
			}
		});

		shading_opz2.addActionListener(new ActionListener() { // Phong Shading

			@Override
			public void actionPerformed(ActionEvent event) {
				MyFrame._rnd.zShading = false;
				for (int i = 0; i < MyFrame._scn.obj.size(); i++) {
					MyFrame._scn.obj.get(i).setBlinn(false);
				}
			}
		});
		shading_opz3.addActionListener(new ActionListener() { // Blinn Shading

			@Override
			public void actionPerformed(ActionEvent event) {
				MyFrame._rnd.zShading = false;
				for (int i = 0; i < MyFrame._scn.obj.size(); i++) {
					MyFrame._scn.obj.get(i).setBlinn(true);
				}
			}
		});

		gouraud_opz1.addActionListener(new ActionListener() { // Gouraud Shading OFF

			@Override
			public void actionPerformed(ActionEvent event) {
				MyFrame._rnd.gouraud = false;
			}
		});

		gouraud_opz2.addActionListener(new ActionListener() { // Gouraud Shading ON

			@Override
			public void actionPerformed(ActionEvent event) {
				MyFrame._rnd.gouraud = true;
			}
		});

		light_opz1.addActionListener(new ActionListener() { // 2nd Light OFF

			@Override
			public void actionPerformed(ActionEvent event) {
				intensity = 0;
				MyFrame._scn.lgt.get(0).setIntensity(intensity);
			}
		});

		light_opz2.addActionListener(new ActionListener() { // 2nd Light ON

			@Override
			public void actionPerformed(ActionEvent event) {
				intensity = 200;
				MyFrame._scn.lgt.get(0).setIntensity(intensity);
			}
		});

		integ_opz1.addActionListener(new ActionListener() { // Runge Kutta

			@Override
			public void actionPerformed(ActionEvent event) {

				Main.eu.kill();

				integ_opz1.setEnabled(false);
				integ_opz1.setSelected(true);

				Main.t = new Thread(Main.rk);
				Main.t.start();

				integ_opz2.setSelected(false);
				integ_opz2.setEnabled(true);
			}
		});

		integ_opz2.addActionListener(new ActionListener() { // Eulero

			@Override
			public void actionPerformed(ActionEvent event) {

				Main.rk.kill();

				integ_opz2.setEnabled(false);
				integ_opz2.setSelected(true);

				Main.t_eu = new Thread(Main.eu);
				Main.t_eu.start();

				integ_opz1.setSelected(false);
				integ_opz1.setEnabled(true);

			}
		});

	

	}

}

class OrthoProjections extends JPanel {// pannello che racchiude le 3 proiezioni ortogonali

	private static final long serialVersionUID = 1L;
	public OrthoPanels orthoX, orthoZ, orthoY;
	public Label pX, pY, pZ, blank1, blank2, blank3;
	public JPanel x = new JPanel(), y = new JPanel(), z = new JPanel();

	public OrthoProjections(int w, int h, Sphere[] pianeti) {

		setBorder(BorderFactory.createLineBorder(Color.black));
		setBounds(850, 80, 500, 600);
		int width = 250;
		int height = 300;
		blank1 = new Label("	 ");
		blank2 = new Label("	 ");
		blank3 = new Label("	 ");

		this.setLayout(new GridLayout(3, 2));

		orthoX = new OrthoPanels(width, height, 'x', pianeti);
		orthoX.setBorder(BorderFactory.createRaisedBevelBorder());
		orthoX.setBorder(BorderFactory.createLoweredBevelBorder());
		x.setLayout(new BorderLayout());
		x.setBorder(BorderFactory.createLoweredBevelBorder());
		pX = new Label("Depth Axis: x");
		pX.setFont(new Font("SansSerif", Font.BOLD, 17));
		x.add(blank1, BorderLayout.WEST);
		x.add(pX, BorderLayout.CENTER);

		orthoZ = new OrthoPanels(width, height, 'z', pianeti);
		orthoZ.setBorder(BorderFactory.createRaisedBevelBorder());
		orthoZ.setBorder(BorderFactory.createLoweredBevelBorder());
		z.setLayout(new BorderLayout());
		z.setBorder(BorderFactory.createLoweredBevelBorder());
		pZ = new Label("Depth Axis: z");
		pZ.setFont(new Font("SansSerif", Font.BOLD, 17));
		z.add(blank2, BorderLayout.WEST);
		z.add(pZ, BorderLayout.CENTER);

		orthoY = new OrthoPanels(width, height, 'y', pianeti);
		orthoY.setBorder(BorderFactory.createRaisedBevelBorder());
		orthoY.setBorder(BorderFactory.createLoweredBevelBorder());
		y.setLayout(new BorderLayout());
		y.setBorder(BorderFactory.createLoweredBevelBorder());
		pY = new Label("Depth Axis: y");
		pY.setFont(new Font("SansSerif", Font.BOLD, 17));
		y.add(blank3, BorderLayout.WEST);
		y.add(pY, BorderLayout.CENTER);

		this.add(orthoX, 0, 0);
		this.add(x, 0, 1);
		this.add(orthoY, 1, 0);
		this.add(y, 1, 1);
		this.add(orthoZ, 2, 0);
		this.add(z, 2, 1);

	}

}

class HelpPanel extends JPanel { // pannello con instruzioni

	private static final long serialVersionUID = 1L;

	JLabel label1, label2, label3, label4, label5;

	public HelpPanel() {

		setBounds(850, 80, 500, 600);
		this.setLayout(new BorderLayout());
		setBorder(BorderFactory.createLineBorder(Color.black));

		Label etichetta = new Label("ISTRUZIONI");
		etichetta.setFont(new Font("SansSerif", Font.BOLD, 17));

		TextArea field = new TextArea();
		field.setText("\nPer cambiare le coordinate del centro e della velocita' dei vari pianeti, "
				+ "\nscegliere il pianeta desiderato nel menu' a tendina e modificare le "
				+ "\ncoordinate tramite gli sliders, " + " \n\nCon le freccette 'su', 'giu', 'dx', 'sx' puoi"
				+ " spostare la visuale e \ncon 'F' e 'B' puoi fare lo zoom \n\nIl sistema di riferimento preso in "
				+ "considerazione e' quello centrato\nnella Terra, quindi in (0,0,1100) ");

		field.setFont(new Font("SansSerif", Font.PLAIN, 14));
		field.setEditable(false);

		add(etichetta, BorderLayout.NORTH);
		add(field, BorderLayout.CENTER);

	}

}

class PlanetsContainerPanel extends JPanel {// pannello container pianeti

	private static final long serialVersionUID = 1L;

	public PlanetsContainerPanel() {
		Label etichetta = new Label("PIANETI");
		etichetta.setFont(new Font("SansSerif", Font.BOLD, 17));

		setBorder(BorderFactory.createLineBorder(Color.black));
		setBounds(850, 80, 500, 600);

		add(etichetta);
	}
}

class PlanetPanel extends JPanel implements ChangeListener { // pannello per modificare i parametri dei pianeti

	private static final long serialVersionUID = 1L;
	String[] planets = new String[] { "Luna", "Marte", "Venere", "Mercurio", "Giove" };
	JComboBox<String> planetsCB = new JComboBox<>(planets);

	int[] val = new int[] { 0, 0, 0, 0, 0, 0 };
	int[] new_val = new int[] { 0, 0, 0, 0, 0, 0 };

	JSlider disLx = new JSlider(-600, 600, 0);
	JSlider disLy = new JSlider(-400, 400, 0);
	JSlider disLz = new JSlider(-1000, 1000, 0);

	JSlider dis2Lx = new JSlider(-9, 9, 0);
	JSlider dis2Ly = new JSlider(-9, 9, 0);
	JSlider dis2Lz = new JSlider(-9, 9, 0);

	public PlanetPanel() {
		setBounds(880, 150, 450, 500);

		Label label = new Label("Scegli il pianeta da modificare");
		Label cX = new Label("centro X");
		Label cY = new Label("centro Y");
		Label cZ = new Label("centro Z");
		Label vX = new Label("velocita' X");
		Label vY = new Label("velocita' Y");
		Label vZ = new Label("velocita' Z");

		JPanel blank = new JPanel();
		blank.setPreferredSize(new Dimension(500, 50));
		JPanel blank1 = new JPanel();
		blank1.setPreferredSize(new Dimension(500, 50));
		JPanel blank2 = new JPanel();
		blank2.setPreferredSize(new Dimension(500, 50));
		JPanel blank3 = new JPanel();
		blank3.setPreferredSize(new Dimension(500, 50));
		JPanel blank4 = new JPanel();
		blank4.setPreferredSize(new Dimension(500, 50));
		JPanel blank5 = new JPanel();
		blank.setPreferredSize(new Dimension(500, 50));
		JPanel blank6 = new JPanel();
		blank.setPreferredSize(new Dimension(500, 50));

		blank.add(planetsCB);
		blank.add(label);
		add(blank);

		blank1.add(cX);
		blank1.add(disLx, "coordinata X del centro");
		add(blank1);

		blank2.add(cY);
		blank2.add(disLy, "coordinata Y del centro");
		add(blank2);

		blank3.add(cZ);
		blank3.add(disLz, "coordinata Z del centro");
		add(blank3);

		blank4.add(vX);
		blank4.add(dis2Lx, "coordinata X della velocita'");
		add(blank4);

		blank5.add(vY);
		blank5.add(dis2Ly, "coordinata Y della velocita'");
		add(blank5);

		blank6.add(vZ);
		blank6.add(dis2Lz, "coordinata Z della velocita'");
		add(blank6);

		label.setFont(new Font("Times New Roman", Font.BOLD, 13));
		cX.setFont(new Font("Times New Roman", Font.BOLD, 13));
		cY.setFont(new Font("Times New Roman", Font.BOLD, 13));
		cZ.setFont(new Font("Times New Roman", Font.BOLD, 13));
		vX.setFont(new Font("Times New Roman", Font.BOLD, 13));
		vY.setFont(new Font("Times New Roman", Font.BOLD, 13));
		vZ.setFont(new Font("Times New Roman", Font.BOLD, 13));

		disLx.setMajorTickSpacing(100);// crea una tacca ogni 100 unita
		disLx.setPaintTicks(true);// disegna tacche tra le varie unita
		disLx.setPaintLabels(true);// scrive i valori sotto lo slider
		disLx.setPreferredSize(new Dimension(300, 40));
		disLx.addChangeListener(this);

		disLy.setMajorTickSpacing(100);
		disLy.setPaintTicks(true);
		disLy.setPaintLabels(true);
		disLy.setPreferredSize(new Dimension(300, 40));
		disLy.addChangeListener(this);

		disLz.setMajorTickSpacing(200);
		disLz.setPaintTicks(true);
		disLz.setPaintLabels(true);
		disLz.setPreferredSize(new Dimension(300, 40));
		disLz.addChangeListener(this);

		dis2Lx.setMajorTickSpacing(1);
		dis2Lx.setPaintTicks(true);
		dis2Lx.setPaintLabels(true);
		dis2Lx.setPreferredSize(new Dimension(300, 40));
		dis2Lx.addChangeListener(this);

		dis2Ly.setMajorTickSpacing(1);
		dis2Ly.setPaintTicks(true);
		dis2Ly.setPaintLabels(true);
		dis2Ly.setPreferredSize(new Dimension(300, 40));
		dis2Ly.addChangeListener(this);

		dis2Lz.setMajorTickSpacing(1);
		dis2Lz.setPaintTicks(true);
		dis2Lz.setPaintLabels(true);
		dis2Lz.setPreferredSize(new Dimension(300, 40));
		dis2Lz.addChangeListener(this);

	}

	@Override
	public void stateChanged(ChangeEvent arg0) {
		
		Point center_new=new Point();
		Point vel_new=new Point();

		String nome = (String) planetsCB.getSelectedItem();
		
		for (int i = 0; i < MyFrame.numPlanets; i++) {

			if (nome==(MyFrame._planets[i].nome)) {
				
				center_new.set(MyFrame._planets[i].center);
				vel_new.set(MyFrame._planets[i].v);

				if (disLx.getValue() != MyFrame._planets[i].center.x && disLx.getValue() != val[0]) {
					center_new.x = disLx.getValue();
					val[0]=disLx.getValue();
				}
				if (disLy.getValue() != MyFrame._planets[i].center.y && disLy.getValue() != val[1]) {
					center_new.y = disLy.getValue();
					val[1]=disLy.getValue();
				}
				if (disLz.getValue() != MyFrame._planets[i].center.z && disLz.getValue() != val[2]) {
					center_new.z = disLz.getValue();
					val[2]=disLz.getValue();
				}
				if (dis2Lx.getValue() != MyFrame._planets[i].v.x && dis2Lx.getValue() != val[3]) {
					vel_new.x = dis2Lx.getValue();
					val[3]=dis2Lx.getValue();
				}
				if (dis2Ly.getValue() != MyFrame._planets[i].v.y && dis2Ly.getValue() != val[4]) {
					vel_new.y = dis2Ly.getValue();
					val[4]=dis2Ly.getValue();
				}
				if (dis2Lz.getValue() != MyFrame._planets[i].v.z && dis2Lz.getValue() != val[5]) {
					vel_new.z = dis2Lz.getValue();
					val[5]=dis2Lz.getValue();
				}
				
				MyFrame._planets[i].center.set(center_new);
				MyFrame._planets[i].v.set(vel_new);
			}
		}

	}
}

class MenuButtonAction implements ActionListener { // action listener dei bottoni del menu'

	public void actionPerformed(ActionEvent event) {

		if (event.getActionCommand().equals("Home")) {

			MyFrame.home_opz.setVisible(true);
			MyFrame.home.setVisible(true);
			MyFrame.o_pj.setVisible(false);
			MyFrame.help.setVisible(false);
			MyFrame.planet.setVisible(false);
			MyFrame.planetH.setVisible(false);

		} else if (event.getActionCommand().equals("OrthoPanels")) {

			MyFrame.home_opz.setVisible(false);
			MyFrame.home.setVisible(false);
			MyFrame.o_pj.setVisible(true);
			MyFrame.help.setVisible(false);
			MyFrame.planet.setVisible(false);
			MyFrame.planetH.setVisible(false);

		} else if (event.getActionCommand().equals("Help")) {

			MyFrame.home_opz.setVisible(false);
			MyFrame.home.setVisible(false);
			MyFrame.o_pj.setVisible(false);
			MyFrame.help.setVisible(true);
			MyFrame.planet.setVisible(false);
			MyFrame.planetH.setVisible(false);

		} else if (event.getActionCommand().equals("Pianeti")) {

			MyFrame.home_opz.setVisible(false);
			MyFrame.home.setVisible(false);
			MyFrame.o_pj.setVisible(false);
			MyFrame.help.setVisible(false);
			MyFrame.planet.setVisible(true);
			MyFrame.planetH.setVisible(true);

		} else if (event.getActionCommand().equals("Exit")) {

			if (!Main.rk.isDead)
				Main.rk.kill();
			if (!Main.eu.isDead)
				Main.eu.kill();
			System.out.println("end");
			System.exit(0);
		}
	}
}
