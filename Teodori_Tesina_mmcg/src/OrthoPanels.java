import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

public class OrthoPanels extends JPanel {
		/*
		  classe dei pannelli delle viste ortogonali. Simula una proiezione ortogonale, creando dei cerchi prendendo
		  a due a due le coordinate dei pianeti, a seconda della vista scelta
		  es: per la vista con asse di profondita' Z, prendo come coordinate del cerchio le coordinate x e y dei pianeti
		 */
		private static final long serialVersionUID = 1L;
		public int width, height;
		public int[] x, y;
		public char a;
		Sphere[] o;

		public OrthoPanels(int w, int h, char axis, Sphere[] pianeti) {
			super();
			width = w;
			height = h;
			x = new int[6];
			y = new int[6];
			a = axis;
			o = pianeti;

			this.setPreferredSize(new Dimension(width, height));
			this.setVisible(true);

		}

		@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			int offsetX = (int) this.getWidth() / 2;
			int offsetY = (int) this.getHeight() / 2;
			int zOffset = (int) o[0].center.z;
			switch (a) {
			case 'x':// zyx
				for (int i = 0; i < o.length; i++) {
					x[i] = widthProp((int) o[i].center.z) + offsetX - widthProp(zOffset);
					y[i] = heightProp((int) o[i].center.y) + offsetY;
				}

				break;
			case 'y':// xzy
				for (int i = 0; i < o.length; i++) {
					x[i] = widthProp((int) o[i].center.x) + offsetX;
					y[i] = heightProp((int) o[i].center.z) + offsetY - heightProp(zOffset);
				}
				break;
			case 'z':// xyz
				for (int i = 0; i < o.length; i++) {
					x[i] = widthProp((int) o[i].center.x) + offsetX;
					y[i] = heightProp((int) o[i].center.y) + offsetY;
				}
				break;
			default:// xyz
				for (int i = 0; i < o.length; i++) {
					x[i] = widthProp((int) o[i].center.x) + offsetX;
					y[i] = heightProp((int) o[i].center.y) + offsetY;
				}
				break;
			}

			for (int i = 0; i < o.length; i++) {
				int r = (int) o[i].size / 3;
				int r_2 = r / 2;
				g.setColor(new Color((float) o[i].color.x, (float) o[i].color.y, (float) o[i].color.z));
				g.fillOval(x[i] - r_2, y[i] - r_2, r, r);
				overlapEarth(i,a, g);

			}

		}

		public void overlapEarth(int i, char axis, Graphics g) {

			int r = (int) o[0].size / 3;
			int r_2 = r / 2;

			switch (axis) {
			case 'x':
				if (o[0].center.x < o[i].center.x) {
					g.setColor(new Color((float) o[0].color.x, (float) o[0].color.y, (float) o[0].color.z));
					g.fillOval(x[0] - r_2, y[0] - r_2, r, r);
				}
				break;

			case 'y':
				if (o[0].center.y < o[i].center.y) {
					g.setColor(new Color((float) o[0].color.x, (float) o[0].color.y, (float) o[0].color.z));
					g.fillOval(x[0] - r_2, y[0] - r_2, r, r);
				}
				break;
				
			case 'z':
				if (o[0].center.z < o[i].center.z) {
					g.setColor(new Color((float) o[0].color.x, (float) o[0].color.y, (float) o[0].color.z));
					g.fillOval(x[0] - r_2, y[0] - r_2, r, r);
				}
				break;

			default:
				if (o[0].center.z < o[i].center.z) {
					g.setColor(new Color((float) o[0].color.x, (float) o[0].color.y, (float) o[0].color.z));
					g.fillOval(x[0] - r_2, y[0] - r_2, r, r);
				}
				break;
			}

		}

		public int widthProp(int x) {
			int x_new;
			x_new = (x * width) / (width * 3);
			x_new /= 2;
			return x_new;
		}

		public int heightProp(int y) {
			int y_new;
			y_new = (y * height) / (height * 3);
			y_new /= 2;
			return y_new;
		}
	}
