//Ilaria Teodori

import java.util.ArrayList;

public class Scene {
	
	/*
		classe contenente tutti gli elementi della scena (luci, pianeti)
	*/

	public ArrayList<Light> lgt; // Lista delle luci nella scena
	public double amb; // Colore della luce ambientale
	public ArrayList<Sphere> obj;

	public Scene(double ambientLight) {
		amb = ambientLight;
		lgt = new ArrayList<Light>();
		obj = new ArrayList<Sphere>();
	}

	public Sphere get(int i) {
		return obj.get(i);
	}

	public void add(Sphere o) {
		obj.add(o);
	}

	public void addLight(Light l) {
		lgt.add(l);
	}

	public int size() {
		return obj.size();
	}

	public Sphere addSphere(Sphere pianeta) {

		this.add(pianeta);
		return this.get(this.size() - 1);
	}

	/*
	 	Scene.simulate() e' un metodo fondamentale per il programma
	 	Per ogni oggetto/pianeta nella scena, va a richiamare il metodo Obj.simulate(), che richiama,
	 	a sua volta, il metodo Obj.shift(Point v), grazie a cui e' possibile animate i pianeti.
	 	Quest'ultimo metodo opera una traslazione su tutti i punti della mesh, prendendo come offset 
	 	le coordinate del vettore velocita'
	 */
	
	public void simulate() {
		for (int i = 0; i < obj.size(); i++) {
			obj.get(i).simulate();
		}
	}


}