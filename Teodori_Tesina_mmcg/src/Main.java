/*

   Copyright 2019 Teodori Ilaria

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.


*/


public class Main {
	
	static final int numPlanets = 6;

	public static Sphere earth=new Sphere(), moon=new Sphere(), venus=new Sphere(), mercury=new Sphere(), jupiter=new Sphere(), mars=new Sphere();
	public static Sphere earth_tmp=new Sphere(), moon_tmp=new Sphere(), venus_tmp=new Sphere(), mercury_tmp=new Sphere(), jupiter_tmp=new Sphere(), mars_tmp=new Sphere();
	static Thread t, t_eu;
	public static Sphere[] pianeti, pianeti_tmp;
	public static double aPrecStart[] = new double[6];
	public static int T=0;
	public static int width = 850;
	public static int height = 680;
	public static long millisOLD;
	public static long millisNEW;
	public static PhysicsEngineRK rk;
	public static PhysicsEngineEuler eu;

	public static void main(String args[]) {

		System.out.println("start");
		millisOLD = System.currentTimeMillis();
		
		pianeti = new Sphere[] { earth, moon, mars, venus, mercury, jupiter };
		pianeti_tmp = new Sphere[] { earth_tmp, moon_tmp, mars_tmp, venus_tmp, mercury_tmp, jupiter_tmp };
		
		Sphere.createPlanets(pianeti, aPrecStart, pianeti_tmp);

		rk = new PhysicsEngineRK(pianeti, pianeti_tmp);
		eu = new PhysicsEngineEuler(pianeti, pianeti_tmp);
	
		t = new Thread(rk);
		
		MyFrame mainWindow = new MyFrame("Simulazione 5 Lune", width, height,pianeti); 
		
		 t.start();
		 
		while (true) {

			millisNEW = System.currentTimeMillis();

			if (millisNEW - millisOLD > 50) {
				millisOLD = millisNEW;

				 mainWindow.repaint(); 
		
				T+=1; //variabile temporale

			}
		}
	}

	

}
