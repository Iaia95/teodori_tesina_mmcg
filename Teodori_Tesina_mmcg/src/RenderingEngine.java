//Ilaria Teodori

import java.util.*;
import java.awt.image.BufferedImage;


public class RenderingEngine{
	public BufferedImage image;
	public Triangle[][] itemBuffer;
	public double[][] zBuffer;
	public int f, w, h, w_2, h_2;
	public Point p1, p2, p3, p4,left,right;
	public Point p[] = new Point[4]; 
	public Point camera, ux, uy, u0;
	public Scene scn;
	public double kDif, kRef;
	public ArrayList<Point> fragments;
	public Sphere object;
	public Point color;
	public boolean texture_mapping, gouraud, zShading = false;
	public int reflection_exponent;
	double u = 0.001;

	public RenderingEngine(int width, int height, int focal, Point camera_, Point normal, Scene scn_){
		//Costruttore del renderer
		w = width;
		h = height;
		f = focal;
		w_2 = w/2;
		h_2 = h/2;
		scn = scn_;
		camera = camera_;
		camera.normal = normal;
		camera.normal.normalize();
		u0 = camera.sum(camera.normal.per(f));
		uy = new Point(0, camera.normal.z, -camera.normal.y);
		uy.normalize();
		ux = uy.vect(camera.normal);
		image = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		zBuffer = new double[w][h];
		itemBuffer = new Triangle[w][h];
		fragments = new ArrayList<Point>();
		texture_mapping = true;
		gouraud = true;
	}
	
	public void setFocal(int focal) {
		f = focal;
		u0 = camera.sum(camera.normal.per(f));
	}
	
	public int getFocal() {
		return f;
	}

	public BufferedImage render(){
		// render() esegue tutte le funzioni necessarie a fere il rendering della scena
		
		// inizializzo la matrice dei pixel con il colore dello sfondo
		// e quella dello z-buffer con la profondita massima
		for(int i=0; i<w; i++)
			for(int j=0; j<h; j++){
				image.setRGB(i, j, 40);
				zBuffer[i][j] = Double.POSITIVE_INFINITY;
			}


		// per ogni triangolo della scena, calcolo il suo rendering
		Point vertex, frag;
		for(int i=0; i<scn.obj.size(); i++){
			object = scn.obj.get(i);
			for(int j=0; j<object.pts.size(); j++){
				vertex = object.pts.get(j);
				frag = vertex.project(camera, f);
				// salvo i valori di illuminazione di ogni vertice nel suo proiettato (fragment)
				setIllumination(vertex, frag);
				fragments.add( frag );
			}
			render(object);
			fragments.clear();
		}
		
		return image;
	}

	public void render(Sphere obj){
		kDif = object.kDif;
		kRef = object.kRef;

		if(!texture_mapping || obj.texture == null) 
			color = obj.color;
		
		for (int i=0; i<obj.trn.size(); i++)
			render(obj.trn.get(i));
	}

	public  void render(Triangle tr){
		// Questa funzione trova la proiezione del triangolo tr sul viewplane
		// e aggiorna i valori delle matrici image e zBuffer
		
		Point n = tr.getNormal(); // normale del triangolo tr
		///////////////////////////////////////////////////////////////////
		// backface culling
		//if( n.dot(tr.a.min(camera)) > 0 ) return;
		////////////////////////////////////////////////////////////////////////		
		/*if( n.dot(camera.normal) > 0 ) return;*/
		
		// carico in p[i] il vertici del triangolo, gia' proiettati sul view port (fragments)
		p[1] = fragments.get(tr.a.id);
		p[2] = fragments.get(tr.b.id);
		p[3] = fragments.get(tr.c.id);
	

		// trasformo i punti nelle coordinate relativa alla camera
		toCameraSpace(p[1]);
		toCameraSpace(p[2]);
		toCameraSpace(p[3]);
		
//////////////////////////////////////////////////////
		// frustum culling
		/*if(
			(Math.abs(p[1].x)>w_2 || Math.abs(p[1].y)>h_2) &&
			(Math.abs(p[2].x)>w_2 || Math.abs(p[2].y)>h_2) &&
			(Math.abs(p[3].x)>w_2 || Math.abs(p[3].y)>h_2) 
		) return;*/

/////////////////////////////////////////////////////////
		// eseguo scambi per far si che p[1] sia il vertice piu' in alto sul viewplane (coordinata y piu bassa),
		// p[2] al centro e p[3] sia il piu basso
		if(p[1].y>p[2].y) swap(1,2);
		if(p[1].y>p[3].y) swap(1,3);
		if(p[2].y>p[3].y) swap(2,3);


		double zSlopeX = -n.x / n.z; // e' l'incremento di z per ogni spostamento di 1 pixel lungo l'asse x
		double zSlopeY = -n.y / n.z; // e' l'incremento di z per ogni spostamento di 1 pixel lungo l'asse y

		/* 
		 	Adesso occorre fare un clipping del triangolo proiettato. Infatti la funzione scan() che disegna
			il triangolo su	image funziona solo se uno dei lati e' parallelo all'asse x del view plane.
			Si opera dunque	un taglio orizzionatle all'altezza del vertice p[2] ottentedo due triangoli (uno
			superiore e	uno inferiore), entrambi con un lato orizzontale (in comune)
		*/
		
		// Caso banale: se due vertici hanno la stessa y, allora un lato e' orizzontale. Posso dunque eseguire
		// direttamente la funzione scan() e disegnare l'intero triangolo

		if(p[1].y == p[2].y){
			if(p[1].x>p[2].x) swap(1,2);
			scan(p[3], p[1], p[2], -1, zSlopeX, zSlopeY, tr);
			return;
		}

		// Nel caso generale devo trovare l'intersezione del triangolo proiettato sul viewplane con
		// la retta orizzontale passante per p[2]. Un punto d'interzezione e' ovviamente p[2], l'altro 
		// si trova sul lato opposto e si ottiene tramite interpolazione lineare
		
		// coefficiente di interpolazione lineare
		double cc = (p[2].y - p[3].y) / (p[1].y - p[3].y);
		p[0] = Point.getInterpolation(cc, p[1], 1-cc, p[3]);

		// Ora bisogna disegnare i due triangoli proiettati sul viewplane (ora entrambi
        // con un lato orizzontale).
		// Opero degli scambi affinche' p[1],p[2],p[0] sia il triangolo superiore e
		// p[3],p[2],p[0] quello inferiore
		if(p[2].x>p[0].x) swap(2,0);

		// si possono ora disegnare le due porzioni di triangolo con la funzione scan()
		scan(p[1], p[2], p[0], 1, zSlopeX, zSlopeY, tr);
		scan(p[3], p[2], p[0], -1, zSlopeX, zSlopeY, tr);
	}


	public void scan(Point p1, Point p2, Point p3, int verse, double zSlopeX, double zSlopeY, Triangle tr){
		/*
			scan() trova, con un procedimento incrementale, tutti i pixel interni del triangolo p1,p2,p3 e per ognuno
		 	di essi richiama draw(), la funzione che aggiorna di fatto le matrici image e zBuffer.
		 	Il valore 'v' indica se si sta operando lo scan di un triangolo superiore (+1) o inferiore (-1) del clipping
		*/

		//  Se si considera la retta che passa lungo il lato sinistro del triangolo,
		// 	ls rappresente l'incremento di x per ogni spostamento di 1 pixel lungo l'asse y del view plane 
		//	(dunque l'inverso del coefficiente angolare)
		double ls = (p1.x - p2.x) / (p1.y - p2.y);

		// rs rappresenta la stessa quantita' per il lato destro del triangolo
		double rs = (p1.x - p3.x) / (p1.y - p3.y);
		
		double x, y, z, xl, xr, zx, c1, c2, c3, u, v, width;
		//int i, j;

		double diffuse, specular;
		int clr;
		//double p1u, p2u, p3u,p1v, p2v, p3v;
		// Nel caso in cui v=+1, si sta facendo lo scan del triangolo superiore, dall'alto verso il basso
		// (dunque dal vertice in alto verso il lato orizzontale)

		double inv_height = 1.0/Math.abs(p2.y - p1.y);
		if(verse==1){
			c1 = 1.0;
			z = p1.z;
			xl = xr = p1.x;
			for(y=p1.y; y<=p3.y; y++){
				// questo for() viene eseguito per ogni riga orizzonatale del triangolo
				zx = z;
				c2 = 1 - c1;
				width = xr - xl;
				for(x=xl; x<=xr; x++){
					//questo for() viene eseguito per ogni pixel della stessa riga
					// con draw(), scrivo sulla matrice di pixel il valore del colore c nel punto x,y del viweplane
					// e aggiorno in tali coordinate anche il valore dello zBuffer
					c3 = 1 - c1 - c2;
					u = c1*p1.u + c2*p2.u + c3*p3.u;
					v = c1*p1.v + c2*p2.v + c3*p3.v;
					if(gouraud){
						diffuse = c1*p1.diffuse + c2*p2.diffuse + (1-c1-c2)*p3.diffuse;
						specular = c1*p1.specular + c2*p2.specular + (1-c1-c2)*p3.specular;
					}
					else{
						diffuse = (p1.diffuse + p2.diffuse + p3.diffuse)/3.0;
						specular = (p1.specular + p2.specular + p3.specular)/3.0;
					}
					clr = getShading(u, v, diffuse, specular).toRGB();
					if(zShading) clr = new Point(-0.3+1000.0/zx).toRGB();
					
					draw(x, y, zx, clr,tr);
					
					// ottengo il nuovo valore di profondita' per la prossima iterazione (pixel di destra)
					zx += zSlopeX;
					c2 -= (1-c1)/width;
				}
				z += ls*zSlopeX + zSlopeY; // ottengo il nuovo valore di profondita' per la prossima iterazione (riga sottostante)
				xl += ls; // ottengo l'estremo sinistro dei valori di x per la prossima riga
				xr += rs; // ottengo l'estremo destro dei valori di x per la prossima riga
				// xl e xr sono gli estremi della riga per il ciclo interno, sono usati nella condizione
				// di permanenza del ciclo for() interno
				c1 -= inv_height;
			}
		}

		// Nel caso in cui v=-1, si sta facendo lo scan del triangolo inferiore, dall'alto verso il basso
		// (dunque dal lato orizzontale verso il vertice in basso)
		// le operazioni sono analoghe
		if(verse==-1){
			c1 = 0.0;
			z = p3.z;
			xl = p2.x;
			xr = p3.x;
			for(y=p3.y; y<=p1.y; y++){
				zx = z;
				c2 = 1 - c1;
				width = xr - xl;
				for(x=xl; x<=xr; x++){

					c3 = 1 - c1 - c2;
					u = c1*p1.u + c2*p2.u + c3*p3.u;
					v = c1*p1.v + c2*p2.v + c3*p3.v;
					if(gouraud){
						diffuse = c1*p1.diffuse + c2*p2.diffuse + (1-c1-c2)*p3.diffuse;
						specular = c1*p1.specular + c2*p2.specular + (1-c1-c2)*p3.specular;
					}
					else{
						diffuse = (p1.diffuse + p2.diffuse + p3.diffuse)/3.0;
						specular = (p1.specular + p2.specular + p3.specular)/3.0;
					}
  					clr = getShading(u, v, diffuse, specular).toRGB();
					if(zShading) clr = new Point(-0.3+1000.0/zx).toRGB();

					draw(x, y, zx, clr,tr);
					zx += zSlopeX;
					c2 -= (1-c1)/width;
				}
				z += ls*zSlopeX + zSlopeY;
				xl += ls;
				xr += rs;
				c1 += inv_height;
			}
		}	
	}

	public  void draw(double xs, double ys, double z, int c, Triangle tr){
		// Se necessario, la funzione draw() aggiorna image con il colore c nel punto (xs,ys)
		// e inoltre aggiorna lo zBuffer nello stesso punto con il valore z

		int x, y;
		// passo dalle coordinate dello spazio a quelle dello schermo (in pixel)
		x = round(xs + w_2);
		y = round(ys + h_2);
		
		// Se le coordinate (x,y) del pixel non sono contenute nel viewplane, esco da draw()
		if( !(x<w && x>=0 && y<h && y>=0) )
			return;
		
		
		/* Se la coordinata z di profondita' e' minore di quella gia' presente nello
			zBuffer, allora aggiorno i valori nelle due matrici con il nuovo colore e la nuova profondita'*/
		double zbuffer = zBuffer[x][y];
		if( (z<zbuffer && z>0) ){
			zBuffer[x][y] = z;
			//itemBuffer[x][y] = tr;
			image.setRGB(x, y, c);
		}
	}


	public  void swap(int i, int j){
		// swap() e' una funzione che scambia i nomi (puntatori) di due oggetti di tipo Point
		Point t;
		t = new Point(p[i]);
		p[i] = new Point(p[j]);
		p[j] = new Point(t);
	}

    
	public void setIllumination(Point p, Point fragment){
		// getShade ottiene l'illuminazione del punto p nella scena, con normale N,
		// calcolando il contributo ambientale, diffusivo (Lambert) e riflessivo (Phong) 
		
		// inizializzazione di variabili
		double intensity; 
		double lambert=0, phong=0, phong_base, diffuse=0, specular=0, d;
		Light light=null;
		double amb = scn.amb; // amb e' il colore della luce ambientale
		Point /*shade ,*/ N = p.normal;
		Point L, V;
		Point o = camera; // centro di proiezione
		Ray shadow_ray;
		boolean visible; 
		for(int i=0; i<scn.lgt.size(); i++){
			// per ogni luce della scena, calcolo il suo contributo d'illuminazione
			visible = true;
			light = scn.lgt.get(i);
			
			// V e' il vettore che congiunge il centro del triangolo al centro di proiezione o
			V = p.to(o);
			
			// L e' il vettore che congiunge il centro del triangolo alla posizione della luce 
			// (chiamato anche 'raggio d'ombra')
			L = light.getDirection(p);
			
			

			if(L.dot(N)<0){
				// Se la superficie del triangolo non 'vede' la luce,
				// non calcolo affatto il contributo di questa sorgente e
				// salto direttamente alla prossima luce
				continue;
			}
			
			shadow_ray = new Ray(light, p);
			for(int j=0; j<scn.obj.size() && visible; j++){
				if(p.ob != scn.obj.get(j))
					visible = shadow_ray.getVisibility(scn, j);
			}

			if(!visible) continue;
			/*
			RenderingEngine shadowCheck = new RenderingEngine(50,50,1, p, L,scn, generation+1);
			print(shadowCheck.render()[25][25].getRed());
			//itemBuffer[w_2][h_2].a.print();
			if(shadowCheck.itemBuffer[25][25] != null) { continue;}
			*/
						// d e' la distanza del triangolo dalla luce 
			intensity = light.i;
			if(light.type != 'd'){
				d = L.norm();
				intensity = light.i * light.getAttenuation(d); // intensity e' l'intensita' luminosa che raggiunge il punto
				L.normalize(); // normalizzo il raggio d'ombra
			}	

			V.normalize(); // normalizzo il raggio di vista
			// L e V sono ora versori

			/* diff e' il contributo d'illuminazione, diffusivo calcolato con la formula
				di Lambert: diff = <L,N>. Notare che a questo punto della funzione diff non
				puo' essere negativo per il controllo effettuato prima */
			lambert = L.dot(N);
			diffuse += lambert*intensity; // incremento il contributo diffusivo

			/* ref e' il contributo di luce riflettente calcolato con la formula
			 	di Phong: ref = 2 <N,L> <N,V> - <L,V> */
			//phong = 2*N.dot(L)*N.dot(V) - L.dot(V); 
			
			// H = L+V/||L+V||   blinn = <H, N>
			
			if(object.blinn){
				phong_base = (L.sum(V).normalized()).dot(N);
				phong_base*=phong_base; phong_base*=phong_base; //Blinn
			}
			else
				phong_base = 2 * N.dot(L) * N.dot(V) - L.dot(V); 

			// se il contributo e' positivo
			if(phong_base>0){
				phong = phong_base;
				for(int j=1; j<object.reflection_exponent; j++)
					phong *= phong_base;
				
				specular += phong*intensity;
			}
		}
		
		diffuse += amb;
		fragment.diffuse = diffuse;
		fragment.specular = specular;
}

	public Point getShading(double u, double v, double diffuse, double specular){
		Point clr;
		if(object.texture != null && texture_mapping){
			clr = object.getColor(u, v);
		}
		else
			clr = color;
		Point shading = clr.per(diffuse*kDif);
		//specular = 0.1 * clr.z/(clr.x+clr.y+clr.z);
		//specular *= Point.clamp( 1.2*(2*clr.z-(clr.x+clr.y)) ,0,1);//(clr.x+clr.y+clr.z);
		specular *= Point.clamp( 2*(clr.z-clr.x-clr.y) ,0,1);//(clr.x+clr.y+clr.z);
		shading.add(specular);
		shading.clamp(0,1);
		return shading;
	}

	public  int round(double x){
		// round() arrotonda valori double all'intero piu' vicino
		// e' utilizzata per passare dalle corrdinate dello spazio (double)
		// alle coordinate della image (int)
		return (int) (Math.round(x));
	}

	public static void print(double x){
		System.out.println(x);
	}

	/*public void toCameraSpace(Point p){
		Point pu = p.min(camera.normal.per(f));
		p.set(p.dot(ux), p.dot(uy), p.z);
	}*/
	public void toCameraSpace(Point p){
		//Point pu = p.min(camera.normal.per(f));
	//	p.set(p.dot(ux), p.dot(uy), p.min(camera).dot(camera.normal) );
		p.set(p.dot(ux), p.dot(uy), p.z);
	}

	public void shiftCamera(Point v){
		camera.shift(v);
		u0.shift(v);
	}
	
	public void rotateCameraSTEP(double theta){
		if(theta!=0) camera.normal = new Point(1,0,0);
	}
	public void rotateCamera(double theta){
		Point N = camera.normal;
		double cos = Math.cos(theta), sin = Math.sin(theta);
		double xr = N.x*cos + N.z*sin;
		double zr = -N.x*sin + N.z*cos;
		camera.normal.set(xr, N.y, zr);

		camera.normal.normalize();

	/*	u0 = camera.sum( camera.normal.per(f) );
		
		ux = uy.vect(camera.normal);
		ux.normalize();*/
		u0 = camera.sum(camera.normal.per(f));
		uy = new Point(0, camera.normal.z, -camera.normal.y);
		uy.normalize();
		ux = uy.vect(camera.normal);
		ux.normalize();
	}

}


