//Ilaria Teodori

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import javax.imageio.ImageIO;

public class Sphere {
	
	static final double g = 23;
	static final int numPlanets = 6;
	
	public ArrayList<Point> pts;
	public ArrayList<Triangle> trn;
	public static boolean c = true;
	public double size;
	public Point center;
	public int n; // numero di paralleli e meridiani
	public double texture_vel;
	public double mass;
	public String nome;
	public Point v;
	public boolean still = false;
	public boolean precessione;
	public double alphaPrec;
	public String texture_name;
	public String texture_stripes;
	public String texture_chess;
	public Point color;
	boolean bool = false;
	public double kDif, kRef;
	public int reflection_exponent = 2;
	public boolean blinn = false;
	public BufferedImage texture;
	ClassLoader classLoader;
	InputStream  is;
	public static BufferedImage clouds, night;

	public static void createPlanets(Sphere[] pianeti, double[] aPrecStart, Sphere[] tmp) {

		//earth
		pianeti[0].makeSphere(90, new Point(0, 0, 1100), 30, "images/world.jpg", "images/world.jpg", "images/world.jpg", 0.1, 1.0, "Terra", 0.41, 2.4);
		pianeti[0].setColor(new Point(0, 0, 1));
		pianeti[0].bool=true;
		// earth.setVelocity(new Point(0.4,0.4,0.4));
		aPrecStart[0]=pianeti[0].alphaPrec;
		pianeti[0].texture_vel = -0.005;
		pianeti[0].still = true;
		pianeti[0].rotateObjectY(pianeti[0].alphaPrec, pianeti[0].center);
		tmp[0].makeSphere(90, new Point(0, 0, 1100), 30, 2.4);

		//moon
		pianeti[1].makeSphere(25, new Point(300, -150, 1050), 30, "images/moon_NASA0.jpg", "images/chess.jpg","images/bw_stripes.jpg", 0.2, 0.8, "Luna", 0.3, 0.9);//
		pianeti[1].setColor(new Point(0.3, 0.6, 1));
		pianeti[1].setVelocity(new Point(0, -5, 0));
		aPrecStart[1]=pianeti[1].alphaPrec;
		pianeti[1].texture_vel = 0.09;
		pianeti[1].rotateObjectY(pianeti[1].alphaPrec, pianeti[1].center);
		tmp[1].makeSphere(25, new Point(300, -150, 1050), 30, 0.9);

		//mars
		pianeti[2].makeSphere(26, new Point(-400, -200, 1050), 30, "images/Mars3.jpg","images/chess_purple.jpg","images/pw_stripes.jpg", 0.2, 0.8, "Marte", -0.4, 0.9);//
		pianeti[2].setColor(new Point(0.2, 0.5, 0.1));
		pianeti[2].setVelocity(new Point(3, 5, 0));
		aPrecStart[2]=pianeti[2].alphaPrec;
		pianeti[2].texture_vel = 0.03;
		pianeti[2].rotateObjectY(pianeti[2].alphaPrec, pianeti[2].center);
		tmp[2].makeSphere(26, new Point(-400, -200, 1050), 30, 0.9);

		//venus
		pianeti[3].makeSphere(26, new Point(250, -160, 1050), 30, "images/Venus2.jpg", "images/chess_red.jpg","images/rw_stripes.jpg", 0.2, 0.8, "Venere", 0.1, 0.9);//
		pianeti[3].setColor(new Point(0.6, 0.5, 0));
		pianeti[3].setVelocity(new Point(10, -10, 0));
		aPrecStart[3]=pianeti[3].alphaPrec;
		pianeti[3].texture_vel = 0.001;
		pianeti[3].rotateObjectY(pianeti[3].alphaPrec, pianeti[3].center);
		tmp[3].makeSphere(26, new Point(250,-160,1050), 30, 0.9);

		//mercury
		pianeti[4].makeSphere(25.5, new Point(-300, -300, 1050), 30, "images/mercury.jpg", "images/chess_blue.jpg","images/blw_stripes.jpg", 0.2, 0.8, "Mercurio", 0.3, 0.8);
		pianeti[4].setColor(new Point(0.6, 0, 0.5));
		pianeti[4].setVelocity(new Point(-10, -4, 0));
		aPrecStart[4]=pianeti[4].alphaPrec;
		pianeti[4].texture_vel = 0.01;
		pianeti[4].rotateObjectY(pianeti[4].alphaPrec, pianeti[4].center);
		tmp[4].makeSphere(25.5, new Point(-300,-300,1050), 30, 0.8);

		//jupiter
		pianeti[5].makeSphere(25, new Point(250, -200, 1050), 30, "images/Jupiter.jpg", "images/chess_green.jpg","images/gw_stripes.jpg", 0.2, 0.8, "Giove", -0.2, 0.9);
		pianeti[5].setColor(new Point(0.2, 0, 0.4));
		pianeti[5].setVelocity(new Point(-5, -7, 0));
		aPrecStart[5]=pianeti[5].alphaPrec;
		pianeti[5].texture_vel = 0.005;
		pianeti[5].rotateObjectY(pianeti[5].alphaPrec, pianeti[5].center);
		tmp[5].makeSphere(25, new Point(250,-200,1050), 30, 0.9);

	}
	
	public Sphere() {
		pts = new ArrayList<Point>();
		trn = new ArrayList<Triangle>();
		texture = null;
		v = new Point(0);

		if (clouds == null)
			try {
				 classLoader = this.getClass().getClassLoader();
				 is = classLoader.getResourceAsStream("images/clouds_new_rs.jpg");
				clouds = ImageIO.read(is);
			} catch (IOException e) {
			}
		if (night == null)
			try {
				classLoader = this.getClass().getClassLoader();
				 is = classLoader.getResourceAsStream("images/world_night.jpg");
				night = ImageIO.read(is);
			} catch (IOException e) {
			}


	}

	public Sphere(ArrayList<Point> points, ArrayList<Triangle> triangles, double k_dif, double k_ref) {
		pts = points;
		trn = triangles;
		kDif = k_dif;
		kRef = k_ref;
		texture = null;
	}

	public void setTexture(String bitmap_name) {
		if (bitmap_name == null) {
			texture = null;
		}

		try {
			classLoader = this.getClass().getClassLoader();
			is = classLoader.getResourceAsStream(bitmap_name);
			texture = ImageIO.read(is);
		} catch (IOException e) {
		}
		if (texture != null)
			return;

		try {
			classLoader = this.getClass().getClassLoader();
			is = classLoader.getResourceAsStream("/images/texture_not_found.png");
			texture = ImageIO.read(is);
		} catch (IOException e) {
		}
	}

	double length(double a, double b, double c, double d) {
		double x = a - c, y = b - d;
		return Math.sqrt(x * x + y * y);
	}

	@Override
	public String toString() {
		return "Obj [size=" + size + ", center=" + center + ", n=" + n + ", mass=" + mass + ", nome=" + nome + ", v="
				+ v + "]";
	}
	
	public Point getColor(double u, double v){
		u = (u % 1.0);
		v = (v % 1.0);
		if(u<0) u+=1.0;
		
		double d = 0;
		int xpixel = (int) ( u*texture.getWidth() );
		int ypixel = (int) ( v*texture.getHeight() );
		
		Color RGB = new Color(texture.getRGB(xpixel, ypixel));
		Point clr = new Point(RGB.getRed(), RGB.getGreen(), RGB.getBlue());
		clr = clr.per(1.0/255);

		// SHADER

  		if( bool ){

			xpixel = (int) ( u*night.getWidth() );
			ypixel = (int) ( v*night.getHeight() );
			RGB = new Color(night.getRGB(xpixel, ypixel));
			Point clrn = new Point(RGB.getRed(), RGB.getGreen(), RGB.getBlue());
			clrn = clrn.per(1.0/255);
			double x = (u+Canvas.t*(0.01+0.000947)) + 0.5;
			x = x%1.0;
			if(x<0) x+=1.0;
			x-=0.5;
			double a = 2*Math.abs(x);
			double b = Math.cos(3*x);
			b *= b; b*=b; b*=b; b*=b;b*=10;
			clr = Point.comb(a,clr,b,clrn);
  			


  			if(c){

  				double uc=u, vc=v, verse=1.0/50;
  				Point p = new Point(u-0.5,-v+0.5,0);
  				p.add( FlowField(p).per( 0.03 ));
  				p.add( FlowField(p).per( 0.03 ));

				uc = p.x-0.3;
				vc = p.y+0.5;
				uc = (uc % 1.0);
				vc = (vc % 1.0);
				if(uc<0) uc+=1.0;
				if(vc<0) vc+=1.0;

		  		Color cloud = new Color(clouds.getRGB( (int) ( uc*clouds.getWidth() ), (int) ( vc*clouds.getHeight() )));
				d = cloud.getRed()/100.0 - 0.2;
				d*=d;
				d = d>1.8? 1.8:d;
				clr.add(d);
			}
		}

		return clr;
	}
	

	public void setColor(Point c) {
		color = c;
	}

	public void setkDif(double x) {
		kDif = x;
	}

	public void setBlinn(boolean b) {
		blinn = b;
	}
	

	public void copy(Sphere cp) {

		this.center =new Point();
		this.color=new Point();

		this.size = cp.size; // r in px
		this.texture_name=cp.texture_name;
		this.texture_chess=cp.texture_chess;
		this.texture_stripes=cp.texture_stripes;
		this.center.set(cp.center);
		this.n = cp.n; // numero di paralleli e meridiani
		this.updatePosition();
		this.mass = cp.mass;
		this.v.set(cp.v);
	
		this.still = cp.still;
		this.nome = cp.nome;
		this.precessione = false;
		
		this.color.set(cp.color);
		this.kDif = cp.kDif;
		this.kRef = cp.kRef;
		this.reflection_exponent = cp.reflection_exponent;
		this.blinn = cp.blinn;
		
		this.setTexture(texture_name);
		this.alphaPrec=cp.alphaPrec;
		this.updatePosition();

	}
	


	public void setkRef(double x) {
		kRef = x;
	}

	public void makeSphere(double r, Point centro, int n, double R, double G, double B, double ref, double dif, double ro) {
		this.makeSphere(r, centro, n, ro);
		color = new Point(R, G, B);
		kDif = dif;
		kRef = ref;
		texture = null;
	}
	
	public void makeSphere(double r, Point centro, int n, String bitmap, String bitmapChess, String bitmapStripes,double ref, double dif, String name,double alpha, double ro) {
		
		this.makeSphere(r, centro, n, 1, 1, 1, ref, dif,ro);
		texture_name=bitmap;
		texture_chess=bitmapChess;
		texture_stripes=bitmapStripes;
		this.setTexture(bitmap);
		this.nome = name;
		this.alphaPrec=alpha;
	}

//	public void makeSphere(double r, Point centro, int n, String bitmap, String bitmapChess, double ref, double dif, String name,double alpha, double ro) {
//	
//		this.makeSphere(r, centro, n, 1, 1, 1, ref, dif,ro);
//		texture_name=bitmap;
//		texture_chess=bitmapChess;
//		this.setTexture(bitmap);
//		this.nome = name;
//		this.alphaPrec=alpha;
//	}

	public void makeSphere(double r, Point centro, int n, double ro) {
		/*
		 * makeSphere() genra una mesh sferica di triangonli la sfera ha raggio r,
		 * centro c ed e' divisa in n paralleli ed n meridiani. In tutto e' composta da
		 * n^2 triangoli. La sfera ha colore (R,G,B) e coefficiente riflessivo e
		 * diffusivo rispettivamente pari a ref e dif.
		 */
	
		mass = r*r*r*ro;
		center = centro;
		size = r;
		this.n = n;
		precessione = false;
		this.updatePts();
		this.updateTrn();
	}

	public void updatePts() {

		pts.clear();

		double r = size;

		double x, y, z;
		double pi = Math.PI;

		Point p;
		int id = 0;
		double theta = 0, phi = 0, dphi = pi / n, dtheta = dphi * 2;
		double cosphi, costheta, sinphi, sintheta;

		for (int i = 0; i < n; i++) {
			sinphi = Math.sin(phi);
			cosphi = Math.cos(phi);
			theta = 0;
			for (int j = 0; j < n; j++) {
				sintheta = Math.sin(theta);
				costheta = Math.cos(theta);
				x = r * sinphi * costheta + center.x;
				z = r * sinphi * sintheta + center.z;
				y = -r * cosphi + center.y;
				p = new Point(x, y, z);
				p.setNormal(center.to(p));
				p.u = theta / (2 * pi);
				p.v = phi / pi;
				p.diffuse = -1.0;
				p.id = id;
				p.ob = this;
					
				pts.add(p);
				
				id++;
				theta += dtheta;
			}
			phi += dphi;
		}

	}

	public void updateTrn() {

		Triangle t;

		trn.clear();

		int a, b, c;
		for (int i = 0; i < n - 1; i++) {
			for (int j = 0; j < n; j++) {
				a = (i * n + j) % (n * n);
				b = ((i + 1) * n + j) % (n * n);
				c = (i * n + j + 1) % (n * n);
				t = new Triangle(pts.get(a), pts.get(b), pts.get(c));
				trn.add(t);

				a = ((i + 1) * n + j) % (n * n);
				b = ((i + 1) * n + j + 1) % (n * n);
				c = (i * n + j + 1) % (n * n);
				t = new Triangle(pts.get(a), pts.get(b), pts.get(c));
				trn.add(t);
			}
		}

	}

	public void updatePosition() {

		this.updatePts();
		this.updateTrn();

	}
	
public void rotateObj(Point L1, Point  center, Sphere tmp) {
		
	Point p_tmp, p_new, _L;
	double _x,_y,_z,_nx,_ny,_nz,zr, yr, xr, nxr, nyr, nzr;

	_L=L1.normalized();
	double costheta=_L.z, sintheta=Math.sqrt(1-costheta*costheta), cosphi=_L.x, sinphi=_L.y;
	
	tmp.center=this.center;
	tmp.updatePts();
	
	for (int i = 0; i < pts.size(); i++) {
		p_tmp = tmp.pts.get(i);
		p_tmp.shift(center.per(-1));
		_x = p_tmp.x * costheta + p_tmp.z * sintheta;
		_y = p_tmp.y;
		_z = -p_tmp.x * sintheta + p_tmp.z * costheta;
		_nx = p_tmp.normal.x * costheta + p_tmp.normal.z * sintheta;
		_ny = p_tmp.normal.y;
		_nz = -p_tmp.normal.x * sintheta + p_tmp.normal.z * costheta;
		
	
		xr = _x * cosphi + _y * sinphi;
		yr = -_x * sinphi + _y * cosphi;
		zr = _z;
		nxr =_nx * cosphi + _ny * sinphi;
		nyr = -_nx * sinphi + _ny * cosphi;
		nzr = _nz;
		
		p_new = this.pts.get(i);
		p_new.shift(center.per(-1));
		p_new.set(xr, yr, zr);
		p_new.normal.set(nxr, nyr, nzr);
		p_new.shift(center);
		
		p_tmp.shift(center);
	}
		
	}

	public void rotateObjectZ(double theta, Point center) {
		Point p;
		double zr, xr, nxr, nzr;
		double cos = Math.cos(theta), sin = Math.sin(theta);
		for (int i = 0; i < pts.size(); i++) {
			p = pts.get(i);
			p.shift(center.per(-1));
			xr = p.x * cos + p.z * sin;
			zr = -p.x * sin + p.z * cos;
			nxr = p.normal.x * cos + p.normal.z * sin;
			nzr = -p.normal.x * sin + p.normal.z * cos;
			p.set(xr, p.y, zr);
			p.normal.set(nxr, p.normal.y, nzr);
			p.shift(center);
		}
	}

	public void rotateObjectY(double theta, Point center) {
		Point p;
		double yr, xr, nxr, nyr;
		double cos = Math.cos(theta), sin = Math.sin(theta);
		for (int i = 0; i < pts.size(); i++) {
			p = pts.get(i);
			p.shift(center.per(-1));
			xr = p.x * cos + p.y * sin;
			yr = -p.x * sin + p.y * cos;
			nxr = p.normal.x * cos + p.normal.y * sin;
			nyr = -p.normal.x * sin + p.normal.y * cos;
			p.set(xr, yr, p.z);
			p.normal.set(nxr, nyr, p.normal.z);
			p.shift(center);
		}
	}

	
	public void rotateTexture(double theta) {
		Point p;

		for (int i = 0; i < pts.size(); i++) {
			p = pts.get(i);
			p.u += theta;
			
		}

	}
	
	public void precessione(double theta) {
		
		this.rotateObjectZ(theta, center);
		this.rotateTexture(theta);
		
	}

	void shift(Point v) {
		// shift() opera una traslazione di vettore v a tutti i punti
		// della scena compresi fra gli indici della lista dei punti della mesh
		for (int i = 0; i < pts.size(); i++) {
			pts.get(i).shift(v); // traslazione del punto
		}
		center.shift(v);
	}

	void scale(double s) {
		Point p;
		for (int i = 0; i < pts.size(); i++) {
			p = pts.get(i);
			p.shift(center.per(-1));
			p.scale(s);
			p.shift(center);
		}
		size *= s;
		mass *= s * s * s;
	}

	public static double smoothstep(double edge0, double edge1, double x) {
		double t;
		t = Point.clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0);
		return t * t * (3.0 - 2.0 * t);
	}

	Point VortF(Point q, Point c) {
		Point d = q.min(c);
		Point result = new Point(d.y, -d.x, 0);
		result = result.per(0.25 / d.dot(d) + 0.05);
		return result;
	}

	Point FlowField(Point q) {
		Point vr, c;
		double dir = 1.;
		c = new Point((MyFrame.canvas.t % 75 - 100) / 60.0, 0.6 * dir, 0);
		vr = new Point(0, 0, 0);
		for (int k = 0; k < 5; k++) {
			vr.add(VortF(q.per(4), c).per(dir));
			c = new Point(c.x + 1., -c.y, 0);
			dir = -dir;
		}
		return vr;
	}

	public void setVelocity(Point p) {
		v = p;
	}

	public void simulate() {
		
	if (!still) 
		this.shift(v);
		
	}

	public void setStill(boolean b) {
		still = b;
	}

	public void setMass(double mass) {
		this.mass = mass;

	}
	


}