//Ilaria Teodori

public class PhysicsEngineRK implements Runnable {

	static final int numPlanets = 6;
	static final double timestep = 0.0003;
	static final double g = 57.5;
	Point cdm;
	double mTot = 0;

	double half_step = timestep / 2;
	double sixth_step = timestep / 6;
	long millisOLD, millisNEW;
	static Point kr1, kr2, kr3, kr4, kv1, kv2, kv3, kv4, c1, c2, c3, c4, pos, vel;
	public Sphere[] pianeti;
	public static Sphere[] tmp;
	static double rTest = 35;
	private boolean isRunning = true;
	public boolean isDead = false;
	public static Point[] poloNord;
	static double a, phi = 0;
	static double vp = -0.05;
	static Point L0, L1, tau, Fg;
	public static double verse;

	public PhysicsEngineRK(Sphere[] planets, Sphere[] planets_tmp) {

		super();
		pianeti = planets;
		tmp=planets_tmp;
		a = 0.0;
		Fg = new Point(0.0);
		vel = new Point(0.0);
		L1 = new Point(0.0);
		L0 = new Point(0.0);
		tau = new Point(0.0);
		poloNord = new Point[numPlanets];
		for (int i = 0; i < numPlanets; i++) {
			poloNord[i] = new Point(0.0);

			poloNord[i].set(pianeti[i].center);
			poloNord[i].y += pianeti[i].size;
			poloNord[i].rotateY(Main.aPrecStart[i]);
		}

	}

	///////////////////////////////////// attrazione di gravita

	public void rk4(Sphere[] pianeti) {

		mTot = 0;
		cdm = new Point();

		//
		for (int i = 0; i < numPlanets; i++) {
			/*
			 * Runge Kutta al 4 ordine per la risoluzione di equazioni differenziali del 1o
			 * ordine. Forza di Attrazione Gravitazionale -> equazione differenziale del 2o
			 * ordine Divido il problema nella risoluzione di 2 equazioni differenziali del
			 * 1st ordine concatenate r' = v && v' = a le condizioni iniziali del sistema
			 * sono i parametri: centri e velocita' dei corpi
			 */
			
			/*
			 Algoritmo di integrazione numerica: Runge Kutta del quarto ordine.
			 L'algoritmo in questione e' un algoritmo iterativo in 4 steps che si occupa della risoluzione
			 di equazioni differenziali di primo grado.
			 Per risolvere il problema della forza di attrazione gravitazionale, che risulta essere un 
			 problema di equazioni differenziali di secondo grado, con l'algoritmo di runge kutta,
			 devo considerare un sistema di 2 equazioni differenziali di primo grado, da calcolare in maniera
			 sequenziale.
			 Essendo l'equazione della forza F=ma, le mie equazioni di primo grado del sistema sono:
			 a=(v)'=dv/dt e v=(r)'=dx/dt
			 Per risolvere le equazioni differenziali, necessito di condizioni iniziali, che, in questo caso,
			 saranno i centri e le velocita' dei corpi dati all'inizio del programma
			 
			 */

			kr1 = new Point();
			kr2 = new Point();
			kr3 = new Point();
			kr4 = new Point();
			kv1 = new Point();
			kv2 = new Point();
			kv3 = new Point();
			kv4 = new Point();
			c1 = new Point();
			c2 = new Point();
			c3 = new Point();
			c4 = new Point();
			pos = new Point();
			vel = new Point();

			c1 = pianeti[i].center;

			for (int iter = 0; iter < 10; iter++) {

				// K1

				// calcolo l'accelerazione
				kv1 = acc(pianeti[i], pianeti, c1);

				// copio le velocita' correnti
				kr1 = pianeti[i].v;
				// uso le velocita per predirre le posizione ad un halfstep nel futuro
				c2 = c1.addP(kv1.per(half_step));
				
				// K2

				// calcolo l'accelerazione
				kv2 = acc(pianeti[i], pianeti, c2);
				// Uso le velocita' per predirre le posizioni mezzo step nel futuro
				kr2 = pianeti[i].v.addP(kr1.per(half_step));
				c3 = c1.addP(kv2.per(half_step));

				// K3

				// calcolo l'accelerazione
				kv3 = acc(pianeti[i], pianeti, c3);
				// Uso le velocita' per predirre le posizioni mezzo step nel futuro
				kr3 = pianeti[i].v.addP(kr2.per(half_step));
				c4 = c1.addP(kv3.per(timestep));

				// K4

				// calcolo l'accelerazione
				kv4 = acc(pianeti[i], pianeti, c4);
				kr4 = pianeti[i].v.addP(kr3.per(timestep));

				// A questo punto, abbiamo 4 velocita' e 4 accelerazioni per ogni corpo
				// Ora combiniamo questi valori per predirre il risultato
				vel.x += sixth_step * (kv1.x + 2 * kv2.x + 2 * kv3.x + kv4.x);
				vel.y += sixth_step * (kv1.y + 2 * kv2.y + 2 * kv3.y + kv4.y);
				vel.z += sixth_step * (kv1.z + 2 * kv2.z + 2 * kv3.z + kv4.z);

				pos.x += sixth_step * (kr1.x + 2 * kr2.x + 2 * kr3.x + kr4.x);
				pos.y += sixth_step * (kr1.y + 2 * kr2.y + 2 * kr3.y + kr4.y);
				pos.z += sixth_step * (kr1.z + 2 * kr2.z + 2 * kr3.z + kr4.z);

			}

			// aggiorno i valori dei pianeti
			pianeti[i].v.add(vel);
			pianeti[i].center.add(pos);
			
			if (pianeti[0].precessione ) {//Precessione
				
				/*
				 la precessione e' approssimata tramite una rotazione della texture in direzione orizzontale rispetto
				 al suo asse di rotazione, sommata ad una composizione di 2 rotazioni che ruotano il pianeta nella
				 posizione del nuovo momento angolare
				 */
				if(i!=0) {
				precession(i, pianeti, kv4.per(pianeti[i].mass));
				}
				
			}
			pianeti[i].rotateTexture(pianeti[i].texture_vel);
		}

	}

	public static Point acc(Sphere o, Sphere bodies[], Point p) {  //metodo per il calcolo dell'accelerazione di gravita' 
		Point acc = new Point(0.0);							// per un singolo pianeta, rispetto a tutti gli altri
		Point i2j = new Point();
		double r, d;

		for (int j = 0; j < 1; j++) {
			if (o.nome.equals(bodies[j].nome)) {
				continue;
			} else {
				i2j = bodies[j].center.to(p);
				r = i2j.norm();
				if (r < rTest) {
					acc.set(0.0, 0.0, 0.0);
					return acc;
				}

				d = -1.0 / (g * r * r);

				acc.x += d * bodies[j].mass * i2j.x;
				acc.y += d * bodies[j].mass * i2j.y;
				acc.z += d * bodies[j].mass * i2j.z;
			}
		}

		return acc;
	}

	// ////////////////////////////////// precessione
	//

	public static void precession(int i, Sphere[] pianeti, Point _Fg) {

		/*
			Per approssimare il calcolo della precessione assiale dei pianeti, mi affido alla teoria del giroscopio.
			Le quantita' che vado a calcolare sono: momento angolare e momento torcente o coppia del pianeta.
			L'equazione del momento angolare L e' L=rXp, con r braccio del momento angolare e p=mv, quantita' di moto,
			mentre l'equazione del momento torcente t (tau) e' t=rxF, con r braccio del momento e F la forza, in questo,
			caso, di attrazione gravitazionale.
			Per calcolare il momento angolare, approssimo il braccio del momento con il vettore che congiunge i due
			poli del pianeta e calcolo la quantita' di moto del pianeta come prodotto della massa del pianeta per la sua 
			velocita' e vado a calcolare il prodotto vettoriale tra questi due vettori.
			Per quanto riguarda il momento torcente, vado a calcolare il prodotto vettoriale del braccio del momento,
			approssimato come sopra, con il vettore della forza di attrazione gravitazionale, in questo caso definito
			come la somma vettoriale di tutte le forze di attrazione gravitazionale subite dal pianeta, rispetto a tutti 
			gli altri.
			Infine, vado a calcolare il mio nuovo momento angolare con l'algoritmo di Eulero, sfruttando l'equazione
			tau=dL/dt in questo modo: L1 = L0+t*tau, con t timestep temporale.
			In questo modo, ricavo l'angolo di precessione dell'asse facendo l'arcocoseno del prodotto scalare tra L0 e L1,
			i momenti angolari prima e dopo il calcolo.
			La precessione e' poi simulata con la rotazione del pianeta rispetto all'angolo di precessione, accompagnata
			con lo scorrimento della texture sul pianeta.

		 */
	
		double _t = 1;
		Fg = _Fg;
		L0 = angularMomentum(i, pianeti);
		tau = torque(i, Fg);
		
		tau = tau.per(_t);
		L1.x = L0.x + tau.x;
		L1.y = L0.y + tau.y;
		L1.z = L0.z + tau.z;
		
		poloNord(i, L0, L1);

		if (i != 0) {
			
			
			pianeti[i].rotateObj(L1, pianeti[i].center, tmp[i]);
		//	pianeti[i].rotate_Obj(L0, L1, pianeti[i].center);
		}

	}

	public static Point torque(int i, Point f) {//momento torcente o coppia

		Point t = new Point(0.0);

		t = (poloNord[i].normalized()).vector((f.normalized()));

		return t;

	}

	public static Point angularMomentum(int i, Sphere[] pianeti) {//momento angolare

		Point L = new Point(0.0);
		Point p = new Point(0.0);
		p = pianeti[i].v.per(pianeti[i].mass);
		L = (poloNord[i].normalized()).vector((p.normalized()));

		return L;
	}


	public static void poloNord(int i, Point L0, Point L1) {//braccio dei momenti

		Point dL = new Point(0.0);

		dL.x = L1.x - L0.x;
		dL.y = L1.y - L0.y;
		dL.z = L1.z - L0.z;

		poloNord[i].x += dL.x;
		poloNord[i].y += dL.y;
		poloNord[i].z += dL.z;
//		poloNord[i].x = L0.x + dL.x;
//		poloNord[i].y = L0.y + dL.y;
//		poloNord[i].z = L0.z + dL.z;

	}

	public void kill() {

		isRunning = false;
		isDead = true;
		Thread.currentThread().interrupt();
	}

	public void startEngine() {
		System.out.println("RK");
		isDead = false;
		while (isRunning) {
			rk4(pianeti);

			try {
				Thread.sleep(60);
			} catch (InterruptedException e) {
			}
		}
		Thread.currentThread().interrupt();
		isRunning = true;
		isDead = true;
	}

	@Override
	public void run() {

		this.startEngine();
	}

}
