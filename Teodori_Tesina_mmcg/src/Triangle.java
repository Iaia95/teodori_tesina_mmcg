//Ilaria Teodori

public class Triangle{
	public Point a, b, c;
	public int IDa, IDb, IDc;


	public Point center;



	public Triangle(Point v1, Point v2, Point v3){
		a=v1;
		b=v2;
		c=v3;

		if(a != null && b != null && c != null)
			center = Point.average(a, b, c);

		if(a.normal != null && b.normal != null && c.normal != null)
			center.normal = Point.average(a.normal, b.normal, c.normal);
		else
			this.makeNormal();
	}

	public Point getNormal(){
		if(center.normal != null)
			return center.normal;
		if(this.updateNormal() != null)
			return center.normal;
		
		return this.makeNormal();
	}

	public Point updateNormal(){
		if(a.normal != null && b.normal != null && c.normal != null){
			center.normal = Point.average(a.normal, b.normal, c.normal);
			return center.normal;
		}
		else return null;
	}

	public Point makeNormal(){
		Point v = a.min(b);
		Point w = c.min(b);
		center.normal = w.vect(v).normalized();
		return center.normal;
	}

	public void print(){
		System.out.println("tr:");
		a.print();
		b.print();
		c.print();
	}

	public Point getCenter(){
		Point cnt = Point.average(a,b,c);
		center = cnt;
		return center;
	}

	public Triangle sum(Point p){
		return new Triangle(a.sum(p), b.sum(p), c.sum(p));
	}
	
	public void shift(Point v){
	    // traslazione di vettore v
	    a.shift(v);
	    b.shift(v);
	    c.shift(v);
	    center = this.getCenter();
  	}


}