//Ilaria Teodori

public class Point {

	// cooridnate del punto nello spazio
	public double x;
	public double y;
	public double z;

	// colore
	public double diffuse;
	public double specular;
	public double u, v;
	// versore normale al punto
	public Point normal;

	public int id;

	public Sphere ob;

	public Point() {
		// costruttore
		this.set(0, 0, 0);
	}

	public Point(double a) {
		// costruttore
		this.set(a, a, a);
	}

	public Point(double a, double b, double c) {
		// costruttore da coordinate
		this.set(a, b, c);
	}

	public Point(Point P) {
		// costruttore che copia un altro punto P
		this.set(P.x, P.y, P.z);
		normal = P.normal;
		diffuse = P.diffuse;
		specular = P.specular;
		u = P.u;
		v = P.v;
	}

	public Point copy(Point P) {
		// costruttore che copia un altro punto P
		Point cp = new Point(P.x, P.y, P.x);
		cp.normal = P.normal;
		cp.diffuse = P.diffuse;
		cp.specular = P.specular;
		cp.u = P.u;
		cp.v = P.v;

		return cp;
	}

	public void set(double a, double b, double c) {
		// set delle coordinate e colore a grigio 70% circa
		x = a;
		y = b;
		z = c;
	}

	public void set(Point P) {
		// set delle coordinate
		x = P.x;
		y = P.y;
		z = P.z;
	}

	public Point sum(Point P) {
		// somma vettoriale
		return new Point(x + P.x, y + P.y, z + P.z);
	}

	public Point sum(double a, double b, double c) {
		// somma vettoriale
		return new Point(x + a, y + b, z + c);
	}

	public void add(Point P) {
		x += P.x;
		y += P.y;
		z += P.z;
	}
	
	public Point addP(Point P) {
		Point t=new Point();
		
		t.x = x + P.x;
		t.y = y + P.y;
		t.z = z + P.z;
		
		return t;
	}

	@Override
	public String toString() {
		return "Point [x=" + x + ", y=" + y + ", z=" + z + "]";
	}

	public void add(double s) {
		x += s;
		y += s;
		z += s;
	}

	public void shift(Point v) {
		// traslazione di vettore v
		x += v.x;
		y += v.y;
		z += v.z;
	}

	public void shift(double a, double b, double c) {
		// traslazione da coordinate
		x += a;
		y += b;
		z += c;
	}

	public Point min(Point P) {
		// differenza vettoriale
		return new Point(x - P.x, y - P.y, z - P.z);
	}

	public Point per(double l) {
		// moltiplicazione per uno scalare l
		return new Point(x * l, y * l, z * l);
	}

	public void scale(double l) {
		x *= l;
		y *= l;
		z *= l;
	}

	public Point star(Point P) {
		return new Point(x * P.x, y * P.y, z * P.z);
	}

	public double dot(Point P) {
		// prodotto scalare
		return x * P.x + y * P.y + z * P.z;
	}

	public double dot(double x_, double y_, double z_) {
		// prodotto scalare
		return x * x_ + y * y_ + z * z_;
	}
	
	public Point vector(Point p){
		Point vect = new Point(0.0);
		
		vect.x=(this.y*p.z)-(this.z*p.y);
		vect.y=(this.z*p.x)-(this.x*p.z);
		vect.z=(this.x*p.y)-(this.y*p.x);
		
		return vect;
	}

	public Point vect(Point P) {
		// prodotto vettoriale
		return new Point(this.y * P.z - this.z * P.y, this.z * P.x - this.x * P.z, this.x * P.y - this.y * P.x);
	}

	public Point to(Point P) {
		// resituisce il vettore applicato che va da "this" al punto P
		return P.min(this);
	}

	public double norm() {
		// norma euclidea
		return Math.sqrt(this.dot(this));
	}

	public double squareNorm() {
		// norma euclidea al quadrato
		return this.dot(this);
	}

	public Point normalized() {
		// noramlizzazione
		double n = this.norm();
		if (n == 0)
			return this;
		return this.per(1 / n);
	}

	public double normalize() {
		// noramlizzazione
		double n = this.norm();
		if (n == 0)
			return 0;
		x /= n;
		y /= n;
		z /= n;
		return n;
	}

	public Point normalizeL1() {
		// normalizzazione secondo la norma l1
		double n = Math.abs(x) + Math.abs(y) + Math.abs(z);
		if (n == 0)
			return this;
		return this.per(1 / n);
	}

	public Point getNormal() {
		// getNormal() restituisce la normale, se esiste
		if (normal == null) {
			System.out.println("Normal is null!:");
			return new Point(0, 0, 0);
		}
		return this.normal;
	}

	public void setNormal(Point v) {
		// imposta normale al punto
		this.normal = v;
		normal.normalize();
	}

	public void clamp(double min, double max) {
		// clamp() costringe i valori del vettore fra min e max
		x = clamp(x, min, max);
		y = clamp(y, min, max);
		z = clamp(z, min, max);
	}

	public static double clamp(double value, double min, double max) {
		// clamp() costringe i valori di un double fra min e max
		if (value < min)
			value = min;
		if (value > max)
			value = max;
		return value;
	}

	public void print() {
		// stampa a schermo delle coordinate del punto
		System.out.println("p(" + x + " " + y + " " + z + ")");
	}

	public Point pow(double p) {
		return new Point(Math.pow(x, p), Math.pow(y, p), Math.pow(x, p));
	}

	public double getDistFrom(Point P) {
		// distanza fra due punti
		Point V = this.to(P);
		return Math.sqrt(V.dot(V));
	}

	public double getSquareDistFrom(Point P) {
		// distanza fra due punti
		Point V = this.to(P);
		return V.dot(V);
	}

	public Point project(double f) {
		/*
		 * project() calcola la proiezione centrale di un punto sul piano z=0 rispetto
		 * al centro (0,0,-f) f rappresenta la distanza del viewplane dal punto di
		 * vista, in termini fotografici e' la lunghezza focale
		 */
		double k = f / (f + z);

		// risultato della matrice di proiezione, conservo la coordinata z dello spazio
		return new Point(k * x, k * y, z);
	}

	public static Point comb(double a, Point A, double b, Point B) {
		// comb calcola la combinazione lineare di due vettori
		// usando i coefficienti a e b.
		return (A.per(a)).sum(B.per(b));
	}

	public static Point getInterpolation(double a, Point A, double b, Point B) {
		// Point p = comb(a,A,b,B);
		Point p = new Point(a * A.x + b * B.x, a * A.y + b * B.y, a * A.z + b * B.z);
		p.u = a * A.u + b * B.u;
		p.v = a * A.v + b * B.v;
		p.diffuse = a * A.diffuse + b * B.diffuse;
		p.specular = a * A.specular + b * B.specular;
		return p;
	}

	public static Point comb(double a, Point A, double b, Point B, double c, Point C) {
		// comb calcola la combinazione lineare di tre vettori
		// usando i coefficienti a,b e c.
		Point p = new Point(a * A.x + b * B.x + c * C.x, a * A.y + b * B.y + c * C.y, a * A.z + b * B.z + c * C.z);
		return p;
	}

	public static Point average(Point P, Point Q) {
		// punto medio fra due punti
		return (P.sum(Q)).per(0.5);
	}

	public static Point average(Point P, Point Q, Point U) {
		// baricentro di tre punti
		return (P.sum(Q).sum(U)).per(1 / 3.0);
	}

	public int toRGB() {
		// normalizzo a valori compresi fra 0 e 255
		Point shade = this.per(255);

		// contengo l'illuminazione fra il valore massimo (255) e minimo (0)
		shade.clamp(0, 255);
		int r = round(shade.x);
		int g = round(shade.y);
		int b = round(shade.z);
		int clr = (r << 16) | (g << 8) | b;
		return clr; // restituisco l'illuminazione finale
	}

	public Point getConvexCoeff(Point v1, Point v2, Point v3) {
		Point l1 = v1.to(this);
		Point l2 = v2.to(this);
		Point l3 = v3.to(this);
		double A3 = (l1.vect(l2)).norm();
		double A2 = (l3.vect(l1)).norm();
		double A1 = (l3.vect(l2)).norm();
		double A = A1 + A2 + A3;

		return new Point(A1 / A, A2 / A, A3 / A);
	}

	public Point getConvexCoeffNew(Point v0, Point v1, Point v2) {
		Point w1 = v1.min(v0);
		Point w2 = v2.min(v0);
		Point q = this.min(v0);
		double alpha = q.dot(w1) / w1.dot(w1);
		double beta = q.dot(w2) / w2.dot(w2);
		return new Point(1 - alpha - beta, alpha, beta);
	}

	public Point getConvexCoeff(Triangle tr) {
		Point v1 = tr.a, v2 = tr.b, v3 = tr.c;
		return this.getConvexCoeff(v1, v2, v3);
	}

	public Point getNormalInterpolation(Point p1, Point p2, Point p3) {
		Point coeff = this.getConvexCoeff(p1, p2, p3);
		double alpha = coeff.x, beta = coeff.y, gamma = coeff.z;
		Point N = Point.comb(alpha, p1.normal, beta, p2.normal, gamma, p3.normal);
		return N;// .normalize();
	}

	public Point getNormalInterpolation(Triangle tr) {
		return this.getNormalInterpolation(tr.a, tr.b, tr.c);
	}

	public Point project(Point camera, double f) {
		Point pc = this.min(camera);
		double depth = pc.dot(camera.normal);
		pc.x *= f / depth;
		pc.y *= f / depth;
		pc.z *= -1;
		// pc.shift(camera);
		Point p = new Point(pc.x, pc.y, depth);
		p.diffuse = diffuse;
		p.specular = specular;
		p.u = u;
		p.v = v;
		return p;

	}

	public Point projectX(Point camera, double f) { // proiezione sul piano x=0
		Point pc = this.min(camera);
		double depth = pc.dot(camera.normal);
		double tmp;
		tmp = pc.x;
		pc.x = pc.z * f / depth; // pc.x *= f / depth;
		pc.y *= f / depth;
		pc.z = tmp * (-1);
		// pc.shift(camera);
		Point p = new Point(pc.x, pc.y, depth);
		p.diffuse = diffuse;
		p.specular = specular;
		p.u = u;
		p.v = v;
		p.id = id;
		return p;

	}

	public Point projectY(Point camera, double f) {
		Point pc = this.min(camera);
		double depth = pc.dot(camera.normal);
		double tmp;
		tmp = pc.y;
		pc.x *= f / depth;
		pc.y = pc.z * f / depth; // pc.y *= f / depth;
		pc.z = tmp * (-1);
		// pc.shift(camera);
		Point p = new Point(pc.x, pc.y, depth);
		p.diffuse = diffuse;
		p.specular = specular;
		p.u = u;
		p.v = v;
		p.id=id;
		return p;

	}

	public void rotateZ(double theta) {
		double yr, xr;
		double cos = Math.cos(theta), sin = Math.sin(theta);
		xr = x * cos + y * sin;
		yr = -x * sin + y * cos;
		x = xr;
		y = yr;
	}


	public void rotateY(double theta) {
		double zr, xr;
		double cos = Math.cos(theta), sin = Math.sin(theta);
		xr = x * cos + z * sin;
		zr = -x * sin + z * cos;
		x = xr;
		z = zr;
	}

	public static int round(double x) {
		// round() arrotonda valori double all'intero piu' vicino
		// e' utilizzata per passare dalle corrdinate dello spazio (double)
		// alle coordinate della pixelMatrix (int)
		return (int) (Math.round(x));
	}

}