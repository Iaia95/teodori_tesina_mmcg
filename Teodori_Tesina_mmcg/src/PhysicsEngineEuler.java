//Ilaria Teodori

public class PhysicsEngineEuler implements Runnable {

	static final int numPlanets = 6;
	static final double timestep = 0.5;
	static final double g = 23;
	Point cdm;
	public double mTot = 0;
	static double vp = -0.005;
	public static double a;
	public static Point Fg;
	public static Point vel;
	public static Point L0;
	public static Point L1;
	public static Point tau;
	public static Point[] poloNord;
	long millisOLD, millisNEW;
	public Sphere[] pianeti;
	public static Sphere[] tmp;
	static double rTest = 35;
	private boolean isRunning = true;
	public boolean isDead = false;
	public static double verse;
	public static Point[] fg_eu = new Point[numPlanets];
	public static Point[] acc2Eu = new Point[2];

	public PhysicsEngineEuler(Sphere[] planets, Sphere[] planets_tmp) {

		super();
		verse = 1.0;
		a = 0.0;
		Fg = new Point(0.0);
		vel = new Point(0.0);
		L1 = new Point(0.0);
		L0 = new Point(0.0);
		tau = new Point(0.0);

		for (int i = 0; i < numPlanets; i++) {
			fg_eu[i] = new Point(0.0);
		}

		for (int i = 0; i < 2; i++) {
			acc2Eu[i] = new Point(0.0);
		}

		//cdm = new Point();
		
		pianeti = planets;
		tmp=planets_tmp;
		poloNord = new Point[numPlanets];
		for (int i = 0; i < numPlanets; i++) {
			poloNord[i] = new Point(0.0);

			poloNord[i].set(pianeti[i].center);
			poloNord[i].y += pianeti[i].size;
			poloNord[i].rotateY(Main.aPrecStart[i]);
		}

	}

	//////////////////////////////// attrazione di gravita
	public void eu(Sphere[] pianeti) {

		/*
		 * Algoritmo di integrazione numerica: Eulero. L'algoritmo in questione e' un
		 * algoritmo che si occupa della risoluzione di equazioni differenziali di primo
		 * grado.
		 * 
		 * Lo pseudo-codice seguente illustra come risolvere il problema della forza di
		 * attrazione gravitazionale, che e' di secondo grado, dividendo il problema in
		 * due equazioni differenziali di primo grado, a cui viene applicato il metodo
		 * di Eulero.
		 * 
		 * v.new = v.old + a * dt pos.new = pos.old + v * dt
		 * 
		 * Per la simulazione nel nostro programma, e' sufficiente calcolare l'equazione
		 * relativa alla velocita', per poi shiftare tutti i punti della mesh prendendo
		 * il vettore velocita' come offset.
		 * 
		 * L'algoritmo di Eulero e' un metodo meno preciso di quello di Runge Kutta al
		 * quarto ordine e cio' comporta una propagazione degli errori molto piu'
		 * significativa, portando i calcoli a divergere e facendo "fuggire" i pianeti
		 * dall'orbita in molto meno tempo.
		 * 
		 */

		for (int i = 0; i < numPlanets; i++) {
			fg_eu[i] = new Point(0.0);
		}

		for (int i = 0; i < 2; i++) {
			acc2Eu[i] = new Point(0.0);
		}

		for (int i = 0; i < numPlanets - 1; i++) {
			for (int j = i + 1; j < numPlanets; j++) {

				acc2Eu = accFg(pianeti[i], pianeti[j]); // attrazione di gravita' con eulero
				fg_eu[i].add(acc2Eu[0].per(pianeti[i].mass));
				fg_eu[j].add(acc2Eu[1].per(pianeti[j].mass));

			}
		}
		

		
		if (pianeti[0].precessione) {  //Precessione
			/*
			 la precessione e' approssimata tramite una rotazione della texture in direzione orizzontale rispetto
			 al suo asse di rotazione, sommata ad una composizione di 2 rotazioni che ruotano il pianeta nella
			 posizione del nuovo momento angolare
			 */
			for (int i = 0; i < numPlanets; i++) {
				if (i != 0) {
					precession(i, pianeti, fg_eu[i].per(pianeti[i].mass));

				} 
			}
		}
		
		for (int i = 0; i < numPlanets; i++) {
			pianeti[i].rotateTexture(pianeti[i].texture_vel);
		}
	}

	public static Point[] accFg(Sphere i, Sphere j) {  
		/*
		 metodo per il calcolo della forza di attrazione gravitazionale, in cui i pianeti sono presi in 
		 considerazione a due a due
		  */
		Point[] acc2 = new Point[2];

		for (int a = 0; a < 2; a++) {
			acc2[a] = new Point(0.0);
		}
		Point i2j = i.center.to(j.center);
		double i2j_n = i2j.normalize();
		double verse = 1.0;

		if (i2j_n < rTest)
			return acc2;
		double denom = 1.0 / (g * i2j_n * i2j_n);

		acc2[0] = i2j.per(timestep * verse * denom * j.mass);
		acc2[1] = i2j.per(timestep * -verse * denom * i.mass);

		i.v.add(acc2[0]);
		j.v.add(acc2[1]);

		return acc2;
	}

	//////////////////////////////////// precessione

	public static void precession(int i, Sphere[] pianeti, Point _Fg) {

		/*
			Per approssimare il calcolo della precessione assiale dei pianeti, mi affido alla teoria del giroscopio.
			Le quantita' che vado a calcolare sono: momento angolare e momento torcente o coppia del pianeta.
			L'equazione del momento angolare L e' L=rXp, con r braccio del momento angolare e p=mv, quantita' di moto,
			mentre l'equazione del momento torcente t (tau) e' t=rxF, con r braccio del momento e F la forza, in questo,
			caso, di attrazione gravitazionale.
			Per calcolare il momento angolare, approssimo il braccio del momento con il vettore che congiunge i due
			poli del pianeta e calcolo la quantita' di moto del pianeta come prodotto della massa del pianeta per la sua 
			velocita' e vado a calcolare il prodotto vettoriale tra questi due vettori.
			Per quanto riguarda il momento torcente, vado a calcolare il prodotto vettoriale del braccio del momento,
			approssimato come sopra, con il vettore della forza di attrazione gravitazionale, in questo caso definito
			come la somma vettoriale di tutte le forze di attrazione gravitazionale subite dal pianeta, rispetto a tutti 
			gli altri.
			Infine, vado a calcolare il mio nuovo momento angolare con l'algoritmo di Eulero, sfruttando l'equazione
			tau=dL/dt in questo modo: L1 = L0+t*tau, con t timestep temporale.
			In questo modo, ricavo l'angolo di precessione dell'asse facendo l'arcocoseno del prodotto scalare tra L0 e L1,
			i momenti angolari prima e dopo il calcolo.
			La precessione e' poi simulata con la rotazione del pianeta rispetto all'angolo di precessione, accompagnata
			con lo scorrimento della texture sul pianeta.

		 */
	
		double _t = 1;
		Fg = _Fg;
		L0 = angularMomentum(i, pianeti);
		tau = torque(i, Fg);
		
		tau = tau.per(_t);
		L1.x = L0.x + tau.x;
		L1.y = L0.y + tau.y;
		L1.z = L0.z + tau.z;
		
		poloNord(i, L0, L1);

		if (i != 0) {
			
			pianeti[i].rotateObj(L1, pianeti[i].center, tmp[i]);
		
		}

	}

	public static Point torque(int i, Point f) {//momento torcente o coppia

		Point t = new Point(0.0);

		t = (poloNord[i].normalized()).vector((f.normalized()));

		return t;

	}

	public static Point angularMomentum(int i, Sphere[] pianeti) {//momento angolare

		Point L = new Point(0.0);
		Point p = new Point(0.0);
		p = pianeti[i].v.per(pianeti[i].mass);
		L = (poloNord[i].normalized()).vector((p.normalized()));

		return L;
	}


	public static void poloNord(int i, Point L0, Point L1) {//braccio dei momenti

		Point dL = new Point(0.0);

		dL.x = L1.x - L0.x;
		dL.y = L1.y - L0.y;
		dL.z = L1.z - L0.z;

		poloNord[i].x += dL.x;
		poloNord[i].y += dL.y;
		poloNord[i].z += dL.z;

	}

	public void kill() {

		isRunning = false;
		isDead = true;
		Thread.currentThread().interrupt();
	}

	public void startEngine() {
		System.out.println("Eu");
		isDead = false;
		while (isRunning) {
			eu(pianeti);

			try {
				Thread.sleep(85);
			} catch (InterruptedException e) {
			}
		}
		Thread.currentThread().interrupt();
		isRunning = true;
		isDead = true;

	}

	@Override
	public void run() {
		this.startEngine();
	}

}
