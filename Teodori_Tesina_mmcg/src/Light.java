//Ilaria Teodori


public class Light extends Point{
	// sorgente luminosa puntiforme

  public double i; // intensita' della sorgete luminosa
  public double q, l, c; // coefficienti di decadimento {quadratico, lineare e costante} */
  public Point direction;
  public char type;
  //public Point color;

  public Light(double x, double y, double z, double intensity){
  	super(x, y, z);
  	i = intensity;
  	c=0;
  	q = 1;
  	l = 0;
  }

  public void setDirectional(Point d){
    type = 'd';
    direction = d.normalized();
    this.set(direction.per(1000));
  }

  public void setIntensity(double intensity){
    i = intensity;
  }

  public Point getDirection(Point p){
    if(type == 'd')
      return direction;
    else return p.to(this);
  }

  public double getAttenuation(double d){
   	// getAtt() restituisce l'attenuazione dovuta dalla distanza, usando i coefficienti
   	// caratteristici della sorgete luminosa

    double att = (c + l*d + q*d*d);
    if(att<1)
        return 1;
    return 1/att;
  }
}